/*****************************************************************************
*
* Project:	gix-avr-sim - AVR simulator
*
* File:     parse_hex.h - AVR ".hex"-files parser file.
*
* Copyright (c) 2016, Gil Treibush <tkks085@gmail.com> 
* ----------------------------------------------------
* This program is licensed under the MIT license.
*   - A copy of the license is included in a "LICENSE" file in the
*     root directory.
*
*	notes:
*       ------
*
*****************************************************************************/

#include<unistd.h>
#include<string.h>
#include<limits.h>
#include<stdio.h>
#include<errno.h>

#include"parse_hex.h"

long int get_hex_data(unsigned char* data, int len){
    char convert_buff[CNVRT_BUFF_SZ];
    char *end;
    long int lr;

    memcpy(convert_buff, data, len); 
    convert_buff[len]   = '\0';
    lr                  = strtol(convert_buff, &end, 16);

    if ((errno == ERANGE && (lr == LONG_MAX || lr == LONG_MIN))
                     || (errno != 0 && lr == 0))
        return -1;

    return lr;
}

int check_crc(struct hex_line *line){

    return 0;
}

parse_ret hex_parse_line(int fd, struct hex_line *hex_line){
    long int res;

    /* Read and parse line header */
    if(read(fd, &hex_line->header, sizeof(hex_line->header)) < 0)
        return stop_err;

    if(hex_line->header.col != ':')
        return stop_err;

    if(!strncmp(hex_line->header.rtyp, RTYPE_EOFL, 2))
        return stop_eof;

    if(strncmp(hex_line->header.rtyp, RTYPE_DATA, 2))
        return stop_err;

    if((res = get_hex_data((unsigned char *)&hex_line->header.bcnt, 2)) < 0)
        return stop_err;

    hex_line->size  = (uint8_t)res;

    if((res = get_hex_data((unsigned char *)&hex_line->header.addr, 4)) < 0 )
        return stop_err;

    hex_line->addr  = (uint16_t)res;

    /* Read data from line */
    if(read(fd, &hex_line->data, (hex_line->size)*2) < 0)
        return stop_err;

    /* Read line CRC */
    if(read(fd, &hex_line->footer, sizeof(hex_line->footer)) < 0)
        return stop_err;

    if(((res = get_hex_data((unsigned char *)&hex_line->footer.crc, 2)) < 0) || 
            strncmp(hex_line->footer.endl, "\r\n", 2))
        return stop_err;

    hex_line->crc  = (uint8_t)res;

    return next;
}

parse_ret map_line(struct hex_line *line, struct mem *mem){
    int i,j;
    char *end;
    unsigned char   bt[3];
    struct mem_sect *mem_sect;
    addr_t          mem_size;
    uint8_t         *mem_ptr;

    bt[2] = '\0';

    mem_sect = mem_get_sect(mem, (addr_t)line->addr);
    if((mem_sect->end_addr - line->addr) < line->size){
        fprintf(stderr, "ERROR: insuficiant free memory in section\n");
        return -1;
    }

    mem_ptr = (uint8_t*)mem_sect->mem;

    for(i=0, j=0; i<(line->size); i++, j+=2){
        memcpy(bt, (line->data + j), 2); 
        mem_ptr[(i + line->addr)] = (uint8_t)strtol(bt, &end, 16);
    }

    return 0;
}

int hex_map_mem(int fd, struct mem *mem){
    struct hex_line hex_line;
    int ret;
    parse_ret line_val;
    int i = 1;

    while((line_val = hex_parse_line(fd, &hex_line)) == next){
        if(map_line(&hex_line, mem)<0){
            fprintf(stderr, "ERROR: Failed to parse line %d of hex file", i);
            return -1;
        }
        i++;
    }

    if (line_val != stop_eof)
        return -1;

    return 0;
}
