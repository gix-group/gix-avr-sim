/*****************************************************************************
*
* Project:	gix-avr-sim - AVR simulator
*
* File:     clocks.c - AVR system timers configuration and API file
*
* Copyright (c) 2016, Gil Treibush <tkks085@gmail.com> 
* ----------------------------------------------------
* This program is licensed under the MIT license.
*   - A copy of the license is included in a "LICENSE" file in the
*     root directory.
*
*	notes:
*   ------
*
*****************************************************************************/

#include <stdio.h>

#include "clocks.h"
#include "flash.h"
#include "eeprom.h"
#include "regs.h"
#include "cu.h"

union clocks CLOCKS = {
    .clks.cpu_clk   = {
        .val    = 0,
        .ticks  = {
            cu_tick,
            sram_tick,
            regs_tick
        },
        .src    = clk_internal_osc
    },

    .clks.io_clk    = {
        .val    = 0,
        .ticks  = {
            NULL,
            NULL,
            NULL
        },
        .src    = clk_internal_osc
    },

    .clks.flash_clk = {
        .val    = 0,
        .ticks  = {
            flash_tick,
            eeprom_tick,
            NULL
        },
        .src    = clk_internal_osc
    },

    .clks.adc_clk   = {
        .val    = 0,
        .ticks  = {
            NULL,
            NULL,
            NULL
        },
        .src    = clk_internal_osc
    }
};

/* Functions */
void init_clocks(){
    /* Initialize clock values, frequences
     * and sources */
}

void start_clocks(){
    int i, j, k;

    printf("\tRunning clocks\n");
    for(i=0; i <= 10; i++){
        for(j=0; j < 4; j++){
            for(k=0; k < TICKS_NUM; k++){
                if(CLOCKS.clks_arr[j].ticks[k])
                    CLOCKS.clks_arr[j].ticks[k]();
            }
        }
    }
}
