/*****************************************************************************
*
* Project:	gix-avr-sim - AVR simulator
*
* File:     cu.c - AVR Control Unit implementation file
*
* Copyright (c) 2016, Gil Treibush <tkks085@gmail.com>
* ----------------------------------------------------
* This program is licensed under the MIT license.
*   - A copy of the license is included in a "LICENSE" file in the
*     root directory.
*
*	notes:
*   ------
*
*****************************************************************************/

#include "opcode.h"
#include "flash.h"
#include "cu.h"
#include "clocks.h"

#include <stdio.h>

/*****************************************************************************
* Global Vars
*****************************************************************************/

/* Main command array */
CMDARR(cmds) = {
    /* Arithmetic/Logic:                       */
    /*    OPCODE, OPMASK, P1MASK, P2MASK, EXEC */
    CMD16(0x0C00, 0xFC00, 0x01F0, 0x020F, add),     // ADD   - Add
    CMD16(0x1C00, 0xFC00, 0x01F0, 0x020F, adc),     // ADC   - Add with Carry
    CMD16(0x9600, 0xFF00, 0x0030, 0x00CF, adiw),    // ADIW  - Add Immediate to Word
    CMD16(0x1800, 0xFC00, 0x01F0, 0x020F, sub),     // SUB   - Subtract Without Carry
    CMD16(0x0800, 0xFC00, 0x01F0, 0x020F, sbc),     // SBC   - Subtract with Carry
    CMD16(0x5000, 0xF000, 0x00F0, 0x0F0F, subi),    // SUBI  - Subtract Immediate
    CMD16(0x4000, 0xF000, 0x00F0, 0x0F0F, sbci),    // SBCI  Subtract Immediate with Carry
                                                    //      SBI  Set Bit in I/O Register
    CMD16(0x9700, 0xFF00, 0x0030, 0x00CF, sbiw),    // SBIW  - Subtract Immediate from Word
#ifdef MUL_INST
    CMD16(0x9C00, 0xFC00, 0x01F0, 0x020F, mul),     // MUL   - Multiply Unsigned
    CMD16(0x0200, 0xFF00, 0x00F0, 0x000F, muls),    // MULS  - Multiply Signed
    CMD16(0x0300, 0xFF88, 0x0070, 0x0007, mulsu),   // MULSU - Multiply Signed with Unsigned
    CMD16(0x0308, 0xFF88, 0x0070, 0x0007, fmul),    // FMUL  - Fractional Multiply Unsigned
    CMD16(0x0380, 0xFF88, 0x0070, 0x0007, fmuls),   // FMULS - Fractional Multiply Signed
    CMD16(0x0388, 0xFF88, 0x0070, 0x0007, fmulsu),  // FMULSU- Fractional Multiply Signed with
#endif
    CMD16(0x9403, 0xFE0F, 0x01F0, 0x0000, inc),     // INC   - Increment
    CMD16(0x940A, 0xFE0F, 0x01F0, 0x0000, dec),     // DEC   - Decrement
    CMD16(0x2000, 0xFC00, 0x01F0, 0x020F, and),     // AND   - Logic AND
    CMD16(0x7000, 0xF000, 0x0F0F, 0x00F0, andi),    // ANDI  - Logic AND with Immediate value
    CMD16(0x2800, 0xFC00, 0x01F0, 0x020F, or),      // OR    - Logical OR
    CMD16(0x6000, 0xF000, 0x00F0, 0x0F0F, ori),     // ORI   - Logical OR with Immediate
    CMD16(0x2400, 0xFC00, 0x01F0, 0x020F, eor),     // EOR   - Exclusive OR
    CMD16(0x9400, 0xFE0F, 0x01F0, 0x0000, com),     // COM   - One's Complement
    CMD16(0x9401, 0xFE0F, 0x01F0, 0x0000, neg),     // NEG   - Twos Complement
    CMD16(0x940B, 0xFF0F, 0x01F0, 0x0000, des),     // DES   - Data Encryption Standard
    /* Jumps:                                  */
    /*    OPCODE, OPMASK, P1MASK, P2MASK, EXEC */
    CMD16(0xC000, 0xF000, 0x0FFF, 0x0000, rjmp),    // RJMP  - Relative Jump
    CMD16(0x9409, 0xFFFF, 0x0000, 0x0000, ijmp),    // IJMP  - Extended Indirect Jump
#ifdef E_JMP
    CMD16(0x9419, 0xFFFF, 0x0000, 0x0000, eijmp),   // EIJMP - Extended Indirect Jump
#endif
#ifdef LONG_JMP
    CMD32(0x940C0000, 0xFE0E0000, 0x01F1FFFF, 0x00000000, jmp),  // JMP - Jump
#endif
    /* Calls:                                  */
    /*    OPCODE, OPMASK, P1MASK, P2MASK, EXEC */
    CMD16(0xD000, 0xF000, 0x0FFF, 0x0000, rcall),   // RCALL - Relative Call to
                                                    //     Subroutine
    CMD16(0x9509, 0xFFFF, 0x0000, 0x0000, icall),   // ICALL - Indirect Call to
                                                    //     Subroutine
    CMD16(0x9519, 0xFFFF, 0x0000, 0x0000, eicall),  // EICALL- Extended Indirect Call
                                                    //     to Subroutine
#ifdef LONG_JMP
    CMD32(0x940E0000, 0xFE0E0000, 0x01F1FFFF, 0x00000000, call), // CALL   - Long Call to a
                                                                 //     Subroutine
#endif
    CMD16(0x9508, 0xFFFF, 0x0000, 0x0000, ret),     // RET   - Return from Subroutine
    CMD16(0x9518, 0xFFFF, 0x0000, 0x0000, reti),    // RETI  - Return from Interrupt
    /* Branch:                                 */
    /*    OPCODE, OPMASK, P1MASK, P2MASK, EXEC */
    CMD16(0xF400, 0xFC00, 0x0007, 0x03F8, brbc),    // BRBC  - Branch if Bit in SREG is Cleared
    CMD16(0xF000, 0xFC00, 0x0007, 0x03F8, brbs),    // BRBS  - Branch if Bit in SREG is Set

    /* Skip:                                   */
    /*    OPCODE, OPMASK, P1MASK, P2MASK, EXEC */
    CMD16(0x1000, 0xFC00, 0x01F0, 0x020F, cpse),    // CPSE  - Compare Skip if Equal
                                                    //      Unsigned
    CMD16(0xFC00, 0xFE08, 0x01F0, 0x0007, sbrc),    // SBRC  - Skip if Bit in Register is
                                                    //      Cleared
    CMD16(0xFE00, 0xFE08, 0x01F0, 0x0007, sbrs),    // SBRS  - Skip if Bit in Register is
                                                    //      Set
    CMD16(0x9900, 0xFF00, 0x00F8, 0x0007, sbic),    // SBIC  - Skip if Bit in I/O Register
                                                    //      is Cleared
    CMD16(0x9B00, 0xFF00, 0x00F8, 0x0007, sbis),    // SBIS  Skip if Bit in I/O Register
                                                    //      is Set
    /* Compare:                                */
    /*    OPCODE, OPMASK, P1MASK, P2MASK, EXEC */
    CMD16(0x9400, 0xFE0F, 0x01F0, 0x0000, cp),      // CP    - Compare
    CMD16(0x0400, 0xFC00, 0x01F0, 0x020F, cpc),     // CPC   - Compare with Carry
    CMD16(0x3000, 0xF000, 0x00F0, 0x0F0F, cpi),     // CPI   - Compare with Immediate
    /* Data transfer:                          */
    /*    OPCODE, OPMASK, P1MASK, P2MASK, EXEC */
    CMD16(0x2C00, 0xFC00, 0x01F0, 0x020F, mov),     // MOV   - Copy Register
    CMD16(0x0100, 0xFF00, 0x00F0, 0x000F, movw),    // MOVW  - Copy Register Word
    /* Store/Load:                             */
    /*    OPCODE, OPMASK, P1MASK, P2MASK, EXEC */
    CMD16(0x900C, 0xFE0F, 0x01F0, 0x0000, ldx),     // LD    - Load Indirect from Data Space to
    CMD16(0x900D, 0xFE0F, 0x01F0, 0x0000, ldx),     //      Register using Index X
    CMD16(0x900E, 0xFE0F, 0x01F0, 0x0000, ldx),
    CMD16(0x8008, 0xFE0F, 0x01F0, 0x0000, ldy),     // LD (LDD) - Load Indirect from Data Space
    CMD16(0x9009, 0xFE0F, 0x01F0, 0x0000, ldy),     //      to Register using Index Y
    CMD16(0x900A, 0xFE0F, 0x01F0, 0x0000, ldy),
    CMD16(0x8008, 0xD208, 0x01F0, 0x2C07, ldy),
    CMD16(0x8000, 0xFE0F, 0x01F0, 0x0000, ldz),     // LD (LDD) - Load Indirect From Data Space
    CMD16(0x9001, 0xFE0F, 0x01F0, 0x0000, ldz),     //      to Register using Index Z
    CMD16(0x9002, 0xFE0F, 0x01F0, 0x0000, ldz),
    CMD16(0x8000, 0xD208, 0x01F0, 0x2C07, ldz),
    CMD16(0xE000, 0xF000, 0x00F0, 0x0F0F, ldi),     // LDI   - Load Immediate
#ifdef LDST_16
    CMD16(0x00A0, 0x00F8, 0xF000, 0x0F07, lds),     // LDS (16-bit) - Load Direct from Data space
#else
    CMD32(0x90000000, 0xFE0F0000, 0x00F00000, 0x0000FFFF, lds),  // LDS - Load Direct from Data space
#endif
    CMD16(0x95C8, 0xFFFF, 0x0000, 0x0000, lpm),     // LPM   - Load Program Memory
    CMD16(0x9004, 0xFE0F, 0x01F0, 0x0000, lpm),
    CMD16(0x9005, 0xFE0F, 0x01F0, 0x0000, lpm),
    CMD16(0x95B8, 0xFFFF, 0x0000, 0x0000, elpm),    // ELPM  - Extended Load Program Memory
    CMD16(0x9006, 0xFE0F, 0x01F0, 0x0000, elpm),
    CMD16(0x9007, 0xFE0F, 0x01F0, 0x0000, elpm),
#ifdef LA_I
    CMD16(0x9205, 0xFE0F, 0x01F0, 0x0000, las),     // LAS   - Load and Set
    CMD16(0x9206, 0xFE0F, 0x01F0, 0x0000, lac),     // LAC   - Load and Clear
    CMD16(0x9207, 0xFE0F, 0x01F0, 0x0000, lat),     // LAT   - Load and Toggle
#endif
#ifdef LDST_16
    CMD16(0xA800, 0xF800, 0x070F, 0x00F0, sts),   // STS (16-bit) - Store Direct to Data Space
#else
    CMD32(0x92000000, 0xFE0F0000, 0x0000FFFF, 0x01F00000, sts),  // STS - Store Direct to data Space
#endif
    CMD16(0x920C, 0xFE0F, 0x01F0, 0x0000, stx),     // ST    - Store Indirect From Register to
    CMD16(0x920D, 0xFE0F, 0x01F0, 0x0000, stx),     //      Data Space using Index X
    CMD16(0x920E, 0xFE0F, 0x01F0, 0x0000, stx),
    CMD16(0x8208, 0xFE0F, 0x01F0, 0x0000, sty),     // ST (STD) - Store Indirect From Register
    CMD16(0x9209, 0xFE0F, 0x01F0, 0x0000, sty),     //      to Data Space using Index Y
    CMD16(0x920A, 0xFE0F, 0x01F0, 0x0000, sty),
    CMD16(0x9208, 0x2D08, 0x01F0, 0x0C07, sty),
    CMD16(0x8200, 0xFE0F, 0x01F0, 0x0000, stz),     // ST (STD) - Store Indirect From Register
    CMD16(0x9201, 0xFE0F, 0x01F0, 0x0000, stz),     //      to Data Space using Index Z
    CMD16(0x9202, 0xFE0F, 0x01F0, 0x0000, stz),
    CMD16(0x8200, 0xD208, 0x01F0, 0x0C07, stz),
    CMD16(0x95E8, 0xFFFF, 0x0000, 0x0000, spm),     // SPM   - Store Program Memory
    CMD16(0x95E8, 0xFFFF, 0x0000, 0x0000, spm2),    // SPM #2- Store Program Memory
    CMD16(0x95F8, 0xFFFF, 0x0000, 0x0000, spm2),    // SPM #2- Store Program Memory
#ifdef XCHN
    CMD16(0x9204, 0xFE0F, 0x01F0, 0x0000, xch),     // XCH   - Exchange
#endif
    CMD16(0xB800, 0xF800, 0x060F, 0x01F0, out),     // OUT   - Store Register to I/O Location
    CMD16(0xB000, 0xF800, 0x01F0, 0x060F, in),      // IN    - Load an I/O Location to Register
    /* Stack Ops:                              */
    /*    OPCODE, OPMASK, P1MASK, P2MASK, EXEC */
    CMD16(0x900F, 0xFE0F, 0x01F0, 0x0000, pop),     // POP   - Pop Register from Stack
    CMD16(0x920F, 0xFE0F, 0x01F0, 0x0000, push),    // PUSH  - Push Register on Stack
    /* Bit Ops:                                */
    /*    OPCODE, OPMASK, P1MASK, P2MASK, EXEC */
    CMD16(0x9405, 0xFE0F, 0x01F0, 0x0000, asr),     // ASR   - Arithmetic Shift Right
    CMD16(0x9406, 0xFE0F, 0x01F0, 0x0000, lsr),     // LSR   - Logical Shift Right
    CMD16(0x9407, 0xFE0F, 0x01F0, 0x0000, ror),     // ROR   - Rotate Right through Carry
    CMD16(0x9402, 0xFE0F, 0x01F0, 0x0000, swap),    // SWAP  - Swap Nibbles
    /* Flags:                                  */
    /*    OPCODE, OPMASK, P1MASK, P2MASK, EXEC */
    CMD16(0x9488, 0xFF8F, 0x0070, 0x0000, bclr),    // BCLR  - Bit Clear in SREG
    CMD16(0xF800, 0xFE08, 0x01F0, 0x0007, bld),     // BLD   - Bit Load from the T
                                                    //      Flag in SREG to a Bit in Register
    CMD16(0xFA00, 0xFE08, 0x01F0, 0x0007, bst),     // BST   - Bit Store from Bit in Register
                                                    //      to T Flag in SREG
    CMD16(0x9408, 0xFF8F, 0x0070, 0x0000, bset),    // BSET  - Bit Set in SREG
    CMD16(0x9800, 0xFF00, 0x00F8, 0x0007, cbi),     // CBI   - Clear Bit in I/O Register
    CMD16(0x9A00, 0xFF00, 0x00F8, 0x0007, sbi),     // SBI   - Set Bit in I/O Register
    CMD16(0x9588, 0xFFFF, 0x0000, 0x0000, slep),    // SLEEP
    CMD16(0x95A8, 0xFFFF, 0x0000, 0x0000, wdr)      // WDR   - Watchdog Reset
};

uint8_t CU_STATE;

#define IF_TRACE() IF_TRACE_PRINT(CU_STATE)

/*****************************************************************************
* API Functions
*****************************************************************************/

void init_cu_state(){
    CLEAR_INST_IGNOR(CU_STATE);
    CLEAR_INST_READY(CU_STATE);
    SET_TRACE_PRINT(CU_STATE);
}

/* CU implementation functions */
int load_instruction(){
    int err;
//  INSTREG     = PC_BASE[PC];
    INSTREG = flash_read_word(PC, &err);
    SET_INST_READY(CU_STATE);

    /* Trace info */
    IF_TRACE()
        printf("Load 0x%.8x: 0x%.4x\n", PC, INSTREG);

    PCINC(1)

    return 0;
}

int do_instruction(){
    int i;
    int cmd_num = (int)(sizeof(cmds)/sizeof(avr_cmd));
    uint16_t oc_num;

    /* Check for special cases */
    if(!INSTREG){
        //TODO: check for a 32 bit command
        //TODO: ignore instruction for 32 bit/nop commands
        nop(0, 0, 0);
    } else {

        /* Instruction skip check*/
        if(INST_IGNOR(CU_STATE)){
            /* Trace info */
            IF_TRACE()
                printf("Instruciton skip\n");
            return 0;
        }

        for(i = 0; i < cmd_num; i++){
            oc_num = cmds[i].cmd_16.oc_mask & INSTREG;
            if(oc_num == cmds[i].cmd_16.cmd_raw){
                cmds[i].cmd_16.exec(    cmds[i].cmd_16.oc_mask & INSTREG,
                                        cmds[i].cmd_16.p1_mask & INSTREG,
                                        cmds[i].cmd_16.p2_mask & INSTREG    );
                break;
            }
        }
    }
    return 0;
}

void cu_tick(){

    printf("- cu: Tick\n");
    if(INST_READY(CU_STATE)){
        do_instruction();
    }

    load_instruction();
}

/*****************************************************************************
* Instruction macros
*****************************************************************************/

/* Helper functions/macros */
/* Flag set */
#define FLG_TEST(COND, FLG) do {    \
if(COND)                            \
    SETFLG(FLG)                     \
else                                \
    CLRFLG(FLG)                     \
} while (0);

#define OVRF_COND_8(PRM1, PRM2, RES)                        \
((PRM1 & (1<<7)) & (PRM2 & (1<<7)) & ~(RES & (1<<7))) ||    \
(~(PRM1 & (1<<7)) & ~(PRM2 & (1<<7)) & (RES & (1<<7)))

#define OVRF_COND_BRW_8(PRM1, PRM2, RES)                    \
((PRM1 & (1<<7)) & ~(PRM2 & (1<<7)) & ~(RES & (1<<7))) ||   \
(~(PRM1 & (1<<7)) & (PRM2 & (1<<7)) & (RES & (1<<7)))

#define HALFC_COND_BRW_8(PRM1, PRM2, RES)               \
(~(PRM1 & (1<<3)) & (PRM2 & (1<<3))) ||                 \
((PRM2 & (1<<3)) & (RES & (1<<3)))   ||                 \
(~(PRM1 & (1<<3)) & (RES & (1<<3)))

#define OVRF_COND_16(PRMH, RES)                         \
(~(PRMH & (1<<7)) & (RES & (1<<15)))

#define CARRY_TEST_8(RES) FLG_TEST((RES & (1<<8)), C)
#define CARRY_TEST_16(RES) FLG_TEST((RES & (1<<16)), C)

#define HALF_CARRY_TEST_8(RES) FLG_TEST((RES & (1<<3)), H)

#define HALF_CARRY_TEST_BRW_8(PRM1, PRM2, RES)          \
FLG_TEST(HALFC_COND_BRW_8(PRM1, PRM2, RES), H)

#define ZERO_TEST_8(RES) FLG_TEST((!((uint8_t)RES)), Z)
#define ZERO_TEST_16(RES) FLG_TEST((!((uint16_t)RES)), Z)

#define NEG_TEST_8(RES) FLG_TEST((RES & (1<<7)), N)
#define NEG_TEST_16(RES) FLG_TEST((RES & (1<<15)), N)

#define OVERF_TEST_8(PRM1, PRM2, RES)       \
FLG_TEST(OVRF_COND_8(PRM1, PRM2, RES), V)

#define OVERF_TEST_BRW_8(PRM1, PRM2, RES)   \
FLG_TEST(OVRF_COND_BRW_8(PRM1, PRM2, RES), V)

#define OVERF_TEST_16(PRMH, RES)            \
FLG_TEST(OVRF_COND_16(PRMH, RES), V)

#define SIGN_TEST() FLG_TEST((FLAG(N)^FLAG(V)), S)

/* Extract params of the form:
 * Rd, Rr -> xxxx-xxrd-dddd-rrrr
 * A, Rr  -> xxxx-xAAr-rrrr-AAAA */
#define EXTRACT_PARAMS_RD_RR(PRM1, PRM2)    \
    PRM1 >>= 4;                             \
    PRM2 = (PRM2 >> 5) | (PRM2 & 0x000F);

/* Extract params of the form:
 * Rd, Rr -> xxxx-xxxx-KKdd-KKKK */
#define EXTRACT_PARAMS_RD2_K(PRM1, PRM2)    \
    PRM1 >>= 4;                             \
    PRM2 = (PRM2 >> 2) | (PRM2 & 0x000F);

/* Extract params of the form:
 * Rd, Rr -> xxxx-KKKK-dddd-KKKK */
#define EXTRACT_PARAMS_RD4_K(PRM1, PRM2)    \
    PRM1 >>= 4;                             \
    PRM2 = (PRM2 >> 4) | (PRM2 & 0x000F);

/* Extract params of the form:
 * Rd, Rr -> xxxx-xxxd-dddd-xxxx */
#define EXTRACT_PARAMS_DR(PRM)              \
    PRM >>= 4;

/* Extract params of the form:
 * P, b -> xxxx-xxxx-AAAA-Abbb */
#define EXTRACT_PARAMS_A(PRM)              \
    PRM >>= 3;

/*****************************************************************************
* instruction implementations
*****************************************************************************/

/* Add */
void add(uint16_t opcode, uint16_t prm1, uint16_t prm2){
    uint32_t result = 0;

    /* Close byte gaps */
    EXTRACT_PARAMS_RD_RR(prm1, prm2)

    result = (uint32_t)(REG_FILE[prm1] + REG_FILE[prm2]);

    /* Trace info */
    IF_TRACE()
        printf("Instruction: ADD R%d + R%d (%d + %d = %d)\n", \
                (int)prm1, (int)prm2, (int)REG_FILE[prm1], (int)REG_FILE[prm2], \
                (int)((uint8_t)result));

    /* Set Flags */
    CARRY_TEST_8(result)
    HALF_CARRY_TEST_8(result)
    ZERO_TEST_8(result)
    NEG_TEST_8(result)
    OVERF_TEST_8(REG_FILE[prm1], REG_FILE[prm2], (uint8_t)result)
    SIGN_TEST()

    REG_FILE[prm1] = (uint8_t)result;

    /* Trace info */
    IF_TRACE()
        print_flags();

    return;
}

void adc(uint16_t opcode, uint16_t prm1, uint16_t prm2){
    uint32_t result = 0;

    /* Close byte gaps */
    EXTRACT_PARAMS_RD_RR(prm1, prm2)

    result = (uint32_t)(REG_FILE[prm1] + REG_FILE[prm2] + FLAG(C));

    /* Trace info */
    IF_TRACE()
        printf("Instruction: ADC R%d + R%d + C (%d + %d + %d = %d)\n", \
                (int)prm1, (int)prm2, (int)REG_FILE[prm1], (int)REG_FILE[prm2], \
                (int)FLAG(C), (int)((uint8_t)result));

    /* Set Flags */
    CARRY_TEST_8(result)
    HALF_CARRY_TEST_8(result)
    ZERO_TEST_8(result)
    NEG_TEST_8(result)
    OVERF_TEST_8(REG_FILE[prm1], REG_FILE[prm2], (uint8_t)result)
    SIGN_TEST()

    REG_FILE[prm1] = result;

    /* Trace info */
    IF_TRACE()
        print_flags();

    return;
}

void adiw(uint16_t opcode, uint16_t prm1, uint16_t prm2){
    uint32_t result = 0;

    /* Close byte gaps */
    EXTRACT_PARAMS_RD2_K(prm1, prm2)

    result = ((uint32_t)(REG_FILE[(prm1*2)+25]) << 8);
    result |= REG_FILE[(prm1*2)+24];
    result += (uint32_t)prm2;

    /* Set Flags */
    CARRY_TEST_16(result)
    ZERO_TEST_16(result)
    NEG_TEST_16(result)
    OVERF_TEST_16(REG_FILE[(prm1*2)+25], result)
    SIGN_TEST()

    /* Trace info */
    IF_TRACE(){
        printf("Instruction: ADIW R%d:R%d + %d (0x%.2x:%.2x + 0x%.2x = 0x%.4x)\n",
                (int)((prm1*2)+24+1), (int)((prm1*2)+24), (int)prm2,
                (int)REG_FILE[(prm1*2)+25], (int)REG_FILE[(prm1*2)+24], (int)prm2,
                (int)((uint16_t)result));
        print_flags();
    }

    REG_FILE[(prm1*2)+24] = (uint8_t)result;
    REG_FILE[(prm1*2)+25] = (uint8_t)(result>>8);

    return;
}

/* Subtract */
void sub(uint16_t opcode, uint16_t prm1, uint16_t prm2){
    uint8_t result = 0;

    EXTRACT_PARAMS_RD_RR(prm1, prm2)

    result = (uint8_t)(REG_FILE[prm1] - REG_FILE[prm2]);

    /* Set Flags */
    OVERF_TEST_BRW_8(prm1, prm2, result)
    ZERO_TEST_8(result)
    NEG_TEST_8(result)
    SIGN_TEST()
    HALF_CARRY_TEST_BRW_8(prm1, prm2, result)
    FLG_TEST(((uint8_t)REG_FILE[prm1] < (uint8_t)REG_FILE[prm2]), C)

    IF_TRACE(){
        printf("Instruction: SUB R%d - R%d (0x%.2x - 0x%.2x = 0x%.2x)\n",
                (int)(prm1), (int)prm2, (int)REG_FILE[prm1], (int)REG_FILE[prm2],
                (int)result);
        print_flags();
    }

    REG_FILE[prm1] = result;

    return;
}

void sbc(uint16_t opcode, uint16_t prm1, uint16_t prm2){
    uint8_t result = 0;

    EXTRACT_PARAMS_RD_RR(prm1, prm2)

    result = (uint8_t)(REG_FILE[prm1] - (REG_FILE[prm2] + FLAG(C)));

    /* Set Flags */
    OVERF_TEST_BRW_8(prm1, prm2, result)
    ZERO_TEST_8(result)
    NEG_TEST_8(result)
    SIGN_TEST()
    HALF_CARRY_TEST_BRW_8(prm1, prm2, result)
    FLG_TEST(((uint8_t)REG_FILE[prm1] < (uint8_t)(REG_FILE[prm2] + FLAG(C))), C)

    IF_TRACE(){
        printf("Instruction: SBC R%d - (R%d + C) (0x%.2x - 0x%.2x = 0x%.2x)\n",
                (int)(prm1), (int)prm2, (int)REG_FILE[prm1], (int)(REG_FILE[prm2] + FLAG(C)),
                (int)result);
        print_flags();
    }

    REG_FILE[prm1] = result;

    return;
}

void subi(uint16_t opcode, uint16_t prm1, uint16_t prm2){
    uint8_t result = 0;

    /* Close byte gaps */
    EXTRACT_PARAMS_RD4_K(prm1, prm2)

    result = (uint8_t)(REG_FILE[16 + prm1] - prm2);

    /* Set Flags */
    OVERF_TEST_BRW_8(prm1, prm2, result)
    ZERO_TEST_8(result)
    NEG_TEST_8(result)
    SIGN_TEST()
    HALF_CARRY_TEST_BRW_8(prm1, prm2, result)
    FLG_TEST(((uint8_t)REG_FILE[16 + prm1] < prm2), C)

    /* Trace info */
    IF_TRACE(){
        printf("Instruction: SUBI R%d - 0x%.2x (0x%.2x - 0x%.2x = 0x%.2x)\n",
                (int)(16 + prm1), (int)prm2, (int)REG_FILE[16 + prm1], (int)prm2,
                (int)result);
        print_flags();
    }

    REG_FILE[16 + prm1] = (uint8_t)result;

    return;
}

void sbci(uint16_t opcode, uint16_t prm1, uint16_t prm2){
    uint8_t result = 0;

    /* Close byte gaps */
    EXTRACT_PARAMS_RD4_K(prm1, prm2)

    result = (uint8_t)(REG_FILE[16 + prm1] - (prm2 + FLAG(C)));

    /* Set Flags */
    OVERF_TEST_BRW_8(prm1, prm2, result)
    ZERO_TEST_8(result)
    NEG_TEST_8(result)
    SIGN_TEST()
    HALF_CARRY_TEST_BRW_8(prm1, prm2, result)
    FLG_TEST(((uint8_t)REG_FILE[16 + prm1] < (uint8_t)(prm2 + FLAG(C))), C)

    /* Trace info */
    IF_TRACE(){
        printf("Instruction: SBCI R%d - (0x%.2x + C) (0x%.2x - 0x%.2x = 0x%.2x)\n",
                (int)(16 + prm1), (int)prm2, (int)REG_FILE[16 + prm1], (int)(prm2 + FLAG(C)),
                (int)result);
        print_flags();
    }

    REG_FILE[16 + prm1] = (uint8_t)result;

    return;
}

void sbiw(uint16_t opcode, uint16_t prm1, uint16_t prm2){
    uint16_t result = 0;

    /* Close byte gaps */
    EXTRACT_PARAMS_RD2_K(prm1, prm2)

    result = ((uint16_t)(REG_FILE[(prm1*2)+25]) << 8);
    result |= REG_FILE[(prm1*2)+24];
    result -= prm2;

    /* Set Flags */
    ZERO_TEST_16(result)
    NEG_TEST_16(result)
    FLG_TEST(((result & (1<<15)) && !(REG_FILE[(prm1*2)+25] & (1<<7))), V)
    SIGN_TEST()
    FLG_TEST((result < (uint16_t)prm2), C)

    /* Trace info */
    IF_TRACE(){
        printf("Instruction: SBIW R%d:R%d - %d (0x%.2x:%.2x - 0x%.2x = 0x%.4x)\n",
                (int)((prm1*2)+25), (int)((prm1*2)+24), (int)prm2,
                (int)REG_FILE[(prm1*2)+25], (int)REG_FILE[(prm1*2)+24], (int)prm2,
                (int)result);
        print_flags();
    }

    REG_FILE[(prm1*2)+24] = (uint8_t)result;
    REG_FILE[(prm1*2)+25] = (uint8_t)(result>>8);

    return;
}

/* Multiply */
#ifdef MUL_INST
void mul(uint16_t opcode, uint16_t prm1, uint16_t prm2){
    return;
}

void muls(uint16_t opcode, uint16_t prm1, uint16_t prm2){
    return;
}

void mulsu(uint16_t opcode, uint16_t prm1, uint16_t prm2){
    return;
}

void fmul(uint16_t opcode, uint16_t prm1, uint16_t prm2){
    return;
}

void fmuls(uint16_t opcode, uint16_t prm1, uint16_t prm2){
    return;
}

void fmulsu(uint16_t opcode, uint16_t prm1, uint16_t prm2){
    return;
}
#endif

/* ++, -- */
void inc(uint16_t opcode, uint16_t prm1, uint16_t prm2){

    /* Close byte gaps */
    EXTRACT_PARAMS_DR(prm1)

    REG_FILE[prm1]++;

    /* Set Flags */
    NEG_TEST_8(REG_FILE[prm1])
    FLG_TEST(((REG_FILE[prm1] - 1) == 0x7F), V)
    ZERO_TEST_8(REG_FILE[prm1])
    SIGN_TEST()

    /* Trace info */
    IF_TRACE(){
        printf("Instruction: INC R%d (0x%.2x)\n",
                prm1, REG_FILE[prm1]);
        print_flags();
    }

    return;
}

void dec(uint16_t opcode, uint16_t prm1, uint16_t prm2){

    /* Close byte gaps */
    EXTRACT_PARAMS_DR(prm1)

    REG_FILE[prm1]--;

    /* Set Flags */
    NEG_TEST_8(REG_FILE[prm1])
    FLG_TEST(((REG_FILE[prm1] + 1) == 0x80), V)
    ZERO_TEST_8(REG_FILE[prm1])
    SIGN_TEST()

    /* Trace info */
    IF_TRACE(){
        printf("Instruction: DEC R%d (0x%.2x)\n",
                prm1, REG_FILE[prm1]);
        print_flags();
    }

    return;
}

/* Arithmetic/Logic Instructions: Logic */

/* Basic logic ops */
void and(uint16_t opcode, uint16_t prm1, uint16_t prm2){
    uint32_t result = 0;

    /* Close byte gaps */
    EXTRACT_PARAMS_RD_RR(prm1, prm2)

    result = REG_FILE[prm1] & REG_FILE[prm2];

    /* Set Flags */
    NEG_TEST_8(result)
    CLRFLG(V)
    ZERO_TEST_8(result)
    SIGN_TEST()

    /* Trace info */
    IF_TRACE(){
        printf("Instruction: AND R%d & R%d (0x%.2x & 0x%.2x = 0x%.2x)\n",
                (int)prm1, (int)prm2, (int)REG_FILE[prm1], (int)REG_FILE[prm2],
                (int)result);
        print_flags();
    }

    REG_FILE[prm1] = (uint8_t)result;

    return;
}

void andi(uint16_t opcode, uint16_t prm1, uint16_t prm2){
    uint32_t result = 0;

    /* Close byte gaps */
    EXTRACT_PARAMS_RD4_K(prm1, prm2)

    result = (uint32_t)(REG_FILE[16 + prm1] & prm2);

    /* Set Flags */
    NEG_TEST_8(result)
    CLRFLG(V)
    ZERO_TEST_8(result)
    SIGN_TEST()

    /* Trace info */
    IF_TRACE(){
        printf("Instruction: ANDI R%d & 0x%.2x (0x%.2x & 0x%.2x = 0x%.2x)\n",
                (int)(16 + prm1), (int)prm2, (int)REG_FILE[16 + prm1], (int)prm2,
                (int)((uint8_t)result));
        print_flags();
    }

    REG_FILE[16 + prm1] = (uint8_t)result;

    return;
}

void or(uint16_t opcode, uint16_t prm1, uint16_t prm2){
    uint32_t result;

    /* Close byte gaps */
    EXTRACT_PARAMS_RD_RR(prm1, prm2)

    result = REG_FILE[prm1] | REG_FILE[prm2];

    /* Set Flags */
    NEG_TEST_8(result)
    CLRFLG(V)
    ZERO_TEST_8(result)
    SIGN_TEST()

    /* Trace info */
    IF_TRACE(){
        printf("Instruction: OR R%d, R%d (0x%.2x | 0x%.2x = 0x%.2x)\n",
                (int)prm1, (int)prm2, (int)REG_FILE[prm1], (int)REG_FILE[prm2],
                (int)result);
        print_flags();
    }

    REG_FILE[prm1] = (uint8_t)result;

    return;
}

void ori(uint16_t opcode, uint16_t prm1, uint16_t prm2){
    uint32_t result;

    /* Close byte gaps */
    EXTRACT_PARAMS_RD4_K(prm1, prm2)

    result = (uint32_t)(REG_FILE[16 + prm1] | prm2);

    /* Set Flags */
    NEG_TEST_8(result)
    CLRFLG(V)
    ZERO_TEST_8(result)
    SIGN_TEST()

    /* Trace info */
    IF_TRACE(){
        printf("Instruction: ORI R%d & 0x%.2x (0x%.2x & 0x%.2x = 0x%.2x)\n",
                (int)(16 + prm1), (int)prm2, (int)REG_FILE[16 + prm1], (int)prm2,
                (int)((uint8_t)result));
        print_flags();
    }

    REG_FILE[16 + prm1] = (uint8_t)result;

    return;
}

void eor(uint16_t opcode, uint16_t prm1, uint16_t prm2){
    uint32_t result;

    /* Close byte gaps */
    EXTRACT_PARAMS_RD_RR(prm1, prm2)

    result = REG_FILE[prm1] ^ REG_FILE[prm2];

    /* Set Flags */
    NEG_TEST_8(result)
    CLRFLG(V)
    ZERO_TEST_8(result)
    SIGN_TEST()

    /* Trace info */
    IF_TRACE(){
        printf("Instruction: EOR R%d, R%d (0x%.2x ^ 0x%.2x = 0x%.2x)\n",
                (int)prm1, (int)prm2, (int)REG_FILE[prm1], (int)REG_FILE[prm2],
                (int)result);
        print_flags();
    }

    REG_FILE[prm1] = (uint8_t)result;

    return;
}

/* One's/Two's Complement */
void com(uint16_t opcode, uint16_t prm1, uint16_t prm2){
    uint8_t result = 0;

    /* Close byte gaps */
    EXTRACT_PARAMS_DR(prm1)

    result  = 0xFF - REG_FILE[prm1];

    /* Set Flags */
    SETFLG(C)
    CLRFLG(V)
    NEG_TEST_8(result)
    ZERO_TEST_8(result)
    SIGN_TEST()

    /* Trace info */
    IF_TRACE(){
        printf("Instruction: COM R%d (0x%.2x) => 0x%.2x\n",
                (int)prm1, (int)REG_FILE[prm1], (int)result);
        print_flags();
    }

    REG_FILE[prm1] = result;

    return;
}

void neg(uint16_t opcode, uint16_t prm1, uint16_t prm2){
    uint8_t result = 0;

    /* Close byte gaps */
    EXTRACT_PARAMS_DR(prm1)

    result  = 0x00 - REG_FILE[prm1];

    /* Set Flags */
    NEG_TEST_8(result)
    ZERO_TEST_8(result)
    HALF_CARRY_TEST_8(result)
    FLG_TEST((REG_FILE[prm1] == 0x80), V)
    FLG_TEST((REG_FILE[prm1]), C)
    SIGN_TEST()

    /* Trace info */
    IF_TRACE(){
        printf("Instruction: NEG R%d (0x%.2x) => 0x%.2x\n",
                (int)prm1, (int)REG_FILE[prm1], (int)result);
        print_flags();
    }

    REG_FILE[prm1] = result;

    return;
}

/* Encryption */

void des(uint16_t opcode, uint16_t prm1, uint16_t prm2){
    return;
}

/* Branch Instructions              */

/* Jump */
void rjmp(uint16_t opcode, uint16_t prm1, uint16_t prm2){
    uint16_t offset = 0;

    CLEAR_INST_READY(CU_STATE);
    if(prm1 & ((uint8_t)1 << 11)){
        offset = ~((prm1 | 0x80) - 1);
        PC -= (uint16_t)offset;
    } else
        PC += prm1;

    /* Trace info */
    IF_TRACE()
        printf("Instruction: RJMP next instruction: 0x%.4x\n", PC);

    return;
}

void ijmp(uint16_t opcode, uint16_t prm1, uint16_t prm2){

    CLEAR_INST_READY(CU_STATE);
    PC = *Z_REG;

    /* Trace info */
    IF_TRACE()
        printf("Instruction: IJMP next instruction: 0x%.4x\n", PC);

    return;
}

#ifdef E_JMP
void eijmp(uint16_t opcode, uint16_t prm1, uint16_t prm2){
    return;
}
#endif

#ifdef LONG_JMP
void jmp(uint32_t opcode, uint32_t prm1, uint32_t prm2){
    return;
}
#endif

/* Calls */

void rcall(uint16_t opcode, uint16_t prm1, uint16_t prm2){
    uint16_t offset = 0;

    SP_PUSH_WORD(PC+1)
    CLEAR_INST_READY(CU_STATE);

    if(prm1 & ((uint8_t)1 << 11)){
        offset = ~((prm1 | 0x80) - 1);
        PC -= (uint16_t)offset;
    } else
        PC += prm1;

    /* Trace info */
    IF_TRACE()
        printf("Instruction: RCALL next instruction: 0x%.4x\n", PC);

    return;
}

void icall(uint16_t opcode, uint16_t prm1, uint16_t prm2){

    SP_PUSH_WORD(PC+1)
    CLEAR_INST_READY(CU_STATE);

    PC = *Z_REG;

    /* Trace info */
    IF_TRACE()
        printf("Instruction: ICALL next instruction: 0x%.4x\n", PC);

    return;
}

void eicall(uint16_t opcode, uint16_t prm1, uint16_t prm2){
    uint32_t addr = 0;

    /* Trace info */
    IF_TRACE()
        printf("Instruction: EICALL\n");

    return;
}

#ifdef LONG_JMP
void call(uint32_t opcode, uint32_t prm1, uint32_t prm2){
    return;
}
#endif

void ret(uint16_t opcode, uint16_t prm1, uint16_t prm2){
    int sp_status;

    //TODO: add status check
    SP_POP_WORD(PC, sp_status)

    /* Trace info */
    IF_TRACE()
        printf("Instruction: RET next instruction: 0x%.4x\n", PC);

    return;
}

void reti(uint16_t opcode, uint16_t prm1, uint16_t prm2){
    int sp_status;

    //TODO: add status check
    SP_POP_WORD(PC, sp_status)

    /* Set Flags */
    SETFLG(I)

    /* Trace info */
    IF_TRACE()
        printf("Instruction: RETI next instruction: 0x%.4x\n", PC);

    return;
}

/* Branch */

void brbc(uint16_t opcode, uint16_t prm1, uint16_t prm2){
    uint8_t offset = 0;

    /* Close byte gaps */
    EXTRACT_PARAMS_A(prm2)

    if(!TESTSREG(prm1)){
        CLEAR_INST_READY(CU_STATE);
        if(prm2 & ((uint8_t)1 << 6)){
            offset = ~((prm2 | 0x80) - 1);
            PC -= (uint16_t)offset;
        } else
            PC += prm2;
    }

    /* Trace info */
    IF_TRACE()
        printf("Instruction: BRBC next instruction: 0x%.4x\n", PC);

    return;
}

void brbs(uint16_t opcode, uint16_t prm1, uint16_t prm2){
    uint8_t offset = 0;

    /* Close byte gaps */
    EXTRACT_PARAMS_A(prm2)

    if(TESTSREG(prm1)){
        CLEAR_INST_READY(CU_STATE);
        if(prm2 & ((uint8_t)1 << 6)){
            offset = ~((prm2 | 0x80) - 1);
            PC -= (uint16_t)offset;
        } else
            PC += prm2;
    }

    /* Trace info */
    IF_TRACE()
        printf("Instruction: BRBS next instruction: 0x%.4x\n", PC);

    return;
}

/* Skip */

void cpse(uint16_t opcode, uint16_t prm1, uint16_t prm2){

    /* Close byte gaps */
    EXTRACT_PARAMS_RD_RR(prm1, prm2)

    if(!(REG_FILE[prm1] - REG_FILE[prm2]))
        SET_INST_IGNOR(CU_STATE);

    /* Trace info */
    IF_TRACE()
        printf("Instruction: CPSE\n");

    return;
}

void sbrc(uint16_t opcode, uint16_t prm1, uint16_t prm2){

    EXTRACT_PARAMS_DR(prm1)

    if(!(REG_FILE[prm1] & (1<<prm2)))
        SET_INST_IGNOR(CU_STATE);

    /* Trace info */
    IF_TRACE()
        printf("Instruction: SBRC\n");

    return;
}

void sbrs(uint16_t opcode, uint16_t prm1, uint16_t prm2){

    EXTRACT_PARAMS_DR(prm1)

    if((REG_FILE[prm1] & (1<<prm2)))
        SET_INST_IGNOR(CU_STATE);

    /* Trace info */
    IF_TRACE()
        printf("Instruction: SBRS\n");

    return;
}

void sbic(uint16_t opcode, uint16_t prm1, uint16_t prm2){

    /* Close byte gaps */
    EXTRACT_PARAMS_A(prm1)

    if(!(IO_REGS[prm1] & (1<<prm2)))
        SET_INST_IGNOR(CU_STATE);

    /* Trace info */
    IF_TRACE()
        printf("Instruction: SBIC\n");

    return;
}

void sbis(uint16_t opcode, uint16_t prm1, uint16_t prm2){

    /* Close byte gaps */
    EXTRACT_PARAMS_A(prm1)

    if((IO_REGS[prm1] & (1<<prm2)))
        SET_INST_IGNOR(CU_STATE);

    /* Trace info */
    IF_TRACE()
        printf("Instruction: SBIS\n");

    return;
}

/* Compare */
void cp(uint16_t opcode, uint16_t prm1, uint16_t prm2){
    uint8_t result = 0;

    EXTRACT_PARAMS_RD_RR(prm1, prm2)

    result = (uint8_t)(REG_FILE[prm1] - REG_FILE[prm2]);

    /* Set Flags */
    OVERF_TEST_BRW_8(prm1, prm2, result)
    ZERO_TEST_8(result)
    NEG_TEST_8(result)
    SIGN_TEST()
    HALF_CARRY_TEST_BRW_8(prm1, prm2, result)
    FLG_TEST(((uint8_t)REG_FILE[prm1] < (uint8_t)REG_FILE[prm2]), C)

    IF_TRACE(){
        printf("Instruction: CP R%d : R%d (0x%.2x - 0x%.2x = 0x%.2x)\n",
                (int)(prm1), (int)prm2, (int)REG_FILE[prm1], (int)REG_FILE[prm2],
                (int)((uint8_t)result));
        print_flags();
    }

    return;
}

void cpc(uint16_t opcode, uint16_t prm1, uint16_t prm2){
    uint8_t result = 0;

    EXTRACT_PARAMS_RD_RR(prm1, prm2)

    result = (uint8_t)(REG_FILE[prm1] - (REG_FILE[prm2] + FLAG(C)));

    /* Set Flags */
    OVERF_TEST_BRW_8(prm1, prm2, result)
    ZERO_TEST_8(result)
    NEG_TEST_8(result)
    SIGN_TEST()
    HALF_CARRY_TEST_BRW_8(prm1, prm2, result)
    FLG_TEST(((uint8_t)REG_FILE[prm1] < (uint8_t)(REG_FILE[prm2] + FLAG(C))), C)

    IF_TRACE(){
        printf("Instruction: CPC R%d - (R%d + C) (0x%.2x - 0x%.2x = 0x%.2x)\n",
                (int)(prm1), (int)prm2, (int)REG_FILE[prm1], (int)(REG_FILE[prm2] + FLAG(C)),
                (int)result);
        print_flags();
    }

    return;
}

void cpi(uint16_t opcode, uint16_t prm1, uint16_t prm2){
    uint8_t result = 0;

    /* Close byte gaps */
    EXTRACT_PARAMS_RD4_K(prm1, prm2)

    result = (uint8_t)(REG_FILE[16 + prm1] - prm2);

    /* Set Flags */
    OVERF_TEST_BRW_8(prm1, prm2, result)
    ZERO_TEST_8(result)
    NEG_TEST_8(result)
    SIGN_TEST()
    HALF_CARRY_TEST_BRW_8(prm1, prm2, result)
    FLG_TEST(((uint8_t)REG_FILE[16 + prm1] < prm2), C)

    /* Trace info */
    IF_TRACE(){
        printf("Instruction: CPI R%d - 0x%.2x (0x%.2x - 0x%.2x = 0x%.2x)\n",
                (int)(16 + prm1), (int)prm2, (int)REG_FILE[16 + prm1], (int)prm2,
                (int)result);
        print_flags();
    }

    return;
}

/* Data Transfer Instructions       */

/* Move */
void mov(uint16_t opcode, uint16_t prm1, uint16_t prm2){

    /* Close byte gaps */
    EXTRACT_PARAMS_RD_RR(prm1, prm2)

    /* Trace info */
    IF_TRACE()
        printf("Instruction: MOV R%d <= R%d (0x%.2x)\n", \
                (int)prm1, (int)prm2, (int)REG_FILE[prm2]);

    REG_FILE[prm1] = REG_FILE[prm2];

    return;
}

void movw(uint16_t opcode, uint16_t prm1, uint16_t prm2){

    EXTRACT_PARAMS_DR(prm1)

    IF_TRACE()
        printf("Instruction: MOVW R%d:R%d <= R%d:R%d (0x%.2x:0x%.2x)\n", \
                (int)((prm1*2) + 1), (int)(prm1*2), (int)((prm2*2) + 1), \
                (int)(prm2*2), (int)REG_FILE[(prm2*2) + 1], (int)REG_FILE[(prm2*2)]);

    REG_FILE[prm1*2] = REG_FILE[prm2*2];
    REG_FILE[(prm1*2)+1] = REG_FILE[(prm2*2)+1];
    return;
}

/* Load */
void ldi(uint16_t opcode, uint16_t prm1, uint16_t prm2){

    /* Close byte gaps */
    EXTRACT_PARAMS_RD4_K(prm1, prm2)

    REG_FILE[prm1+16] = (uint8_t)prm2;

    /* Trace info */
    IF_TRACE()
        printf("Instruction: LDI R%d <- 0x%.4x\n", (prm1+16), prm2);

    return;
}

#ifdef LDST_16
void lds(uint16_t opcode, uint16_t prm1, uint16_t prm2){

    printf(" Lds16\n");
    return;
}
#else
void lds(uint32_t opcode, uint32_t prm1, uint32_t prm2){

    printf(" Lds\n");
    return;
}
#endif

void ldx(uint16_t opcode, uint16_t prm1, uint16_t prm2){

    printf(" Ldx\n");
    return;
}

void ldy(uint16_t opcode, uint16_t prm1, uint16_t prm2){

    printf(" Ldy\n");
    return;
}

void ldz(uint16_t opcode, uint16_t prm1, uint16_t prm2){

    printf(" Ldz\n");
    return;
}

/* Store */
#ifdef LDST_16
void sts(uint16_t opcode, uint16_t prm1, uint16_t prm2){

    printf(" Sts16\n");
    return;
}
#else
void sts(uint32_t opcode, uint32_t prm1, uint32_t prm2){

    printf(" Sts\n");
    return;
}
#endif

void stx(uint16_t opcode, uint16_t prm1, uint16_t prm2){
//    uint8_t s_type = 0;
//
//    EXTRACT_PARAMS_DR(prm1)
//
//    sram_write_byte(*X_REG, REG_FILE[prm1]);
//
//    s_type = opcode & 0x0F;
//    switch(s_type){
//        case 0x0d:
//            // ST X+, Rr
//            *X_REG++;
//            break;
//        case 0x0e:
//            // ST -X, Rr
//            *X_REG--;
//            break;
//    }
//
//    /* Trace info */
//    IF_TRACE()
//        printf("Instruction: ST [0x%.4x] <- R%d (0x%.2x)\n", \
//                (int)(*X_REG), (int)prm1, (int)REG_FILE[prm1]);

    return;
}

void sty(uint16_t opcode, uint16_t prm1, uint16_t prm2){
    uint16_t s_type = 0;
    uint16_t dspl   = 0;

    EXTRACT_PARAMS_DR(prm1)

    s_type = opcode & 0x100F;
    switch(s_type){
        case 0x0008:
            // ST Y, Rr / STD Y+q, Rr

            break;
        case 0x1009:
            // ST Y+, Rr

            break;
        case 0x100A:
            // ST -Y, Rr

            break;
    }

    /* Trace info */
    IF_TRACE()
        printf("Instruction: ST [0x%.4x] <- R%d (0x%.2x)\n", \
                (int)(*Y_REG), (int)prm1, (int)REG_FILE[prm1]);

    return;
}

void stz(uint16_t opcode, uint16_t prm1, uint16_t prm2){
    uint16_t s_type = 0;
    uint16_t dspl   = 0;

    s_type = opcode & 0x100F;
    switch(s_type){
        case 0x0008:
            // ST Y, Rr / STD Y+q, Rr

            break;
        case 0x1009:
            // ST Y+, Rr

            break;
        case 0x100A:
            // ST -Y, Rr

            break;
    }

    /* Trace info */
    IF_TRACE()
        printf("Instruction: ST [0x%.4x] <- R%d (0x%.2x)\n",
                (int)9, (int)9, (int)9);

    return;
}

/* Prog mem */

void lpm(uint16_t opcode, uint16_t prm1, uint16_t prm2){

    printf(" Lpm\n");
    return;
}

void elpm(uint16_t opcode, uint16_t prm1, uint16_t prm2){

    /* Trace info */
    IF_TRACE()
        printf("Instruction: ELPM\n");

    return;
}

void spm(uint16_t opcode, uint16_t prm1, uint16_t prm2){

    /* Trace info */
    IF_TRACE()
        printf("Instruction: SPM\n");

    return;
}

void spm2(uint16_t opcode, uint16_t prm1, uint16_t prm2){

    /* Trace info */
    IF_TRACE()
        printf("Instruction: SPM\n");

    return;
}

/* Data mem */
#ifdef LA_I 
void las(uint16_t opcode, uint16_t prm1, uint16_t prm2){
    return;
}

void lac(uint16_t opcode, uint16_t prm1, uint16_t prm2){
    return;
}

void lat(uint16_t opcode, uint16_t prm1, uint16_t prm2){
    return;
}
#endif
#ifdef XCHN
void xch(uint16_t opcode, uint16_t prm1, uint16_t prm2){
    return;
}
#endif

/* I/O */
void out(uint16_t opcode, uint16_t prm1, uint16_t prm2){

    EXTRACT_PARAMS_RD_RR(prm2, prm1)

    /* Trace info */
    IF_TRACE()
        printf("Instruction: OUT IOR%d <= R%d 0x%.2x\n", \
                (int)prm1, (int)prm2, (int)REG_FILE[prm2]);

    IO_REGS[prm1] = REG_FILE[prm2];

    return;
}

void in(uint16_t opcode, uint16_t prm1, uint16_t prm2){

    EXTRACT_PARAMS_RD_RR(prm1, prm2)

    /* Trace info */
    IF_TRACE()
        printf("Instruction: OUT R%d <= IOR%d 0x%.2x\n", \
                (int)prm1, (int)prm2, (int)IO_REGS[prm2]);

    REG_FILE[prm1] = IO_REGS[prm2];

    return;
}

/* Stack ops */
void push(uint16_t opcode, uint16_t prm1, uint16_t prm2){

    EXTRACT_PARAMS_DR(prm1)

    SP_PUSH_DWORD(REG_FILE[prm1])

    /* Trace info */
    IF_TRACE()
        printf("Instruction: PUSH 0x%.2x\n", REG_FILE[prm1]);

    return;
}

void pop(uint16_t opcode, uint16_t prm1, uint16_t prm2){

//    EXTRACT_PARAMS_DR(prm1)
//
//    SP_POP_DWORD(REG_FILE[prm1])
//
//    /* Trace info */
//    IF_TRACE()
//        printf("Instruction: POP 0x%.2x\n", REG_FILE[prm1]);
//
//    return;
}

/* Bit and Bit-test                 */

/* Shift/Rotate */
void lsr(uint16_t opcode, uint16_t prm1, uint16_t prm2){
    uint32_t result = 0;

    EXTRACT_PARAMS_DR(prm1)

    result = (uint32_t)REG_FILE[prm1];
    result &= ~((uint32_t)(1<<8));

    /* Set C Flag */
    FLG_TEST((result & (uint32_t)1), C)

    result >>= 1;

    /* Set Flags */
    CLRFLG(N)
    ZERO_TEST_8(result)
    SIGN_TEST()
    FLG_TEST((FLAG(N)^FLAG(C)), V)

    /* Trace info */
    IF_TRACE(){
        printf("Instruction: LSR R%d (0x%.2x) => 0x%.2x\n",
                (int)prm1, REG_FILE[prm1], (int)result);
        print_flags();
    }

    REG_FILE[prm1] = (uint8_t)result;

    return;
}

void ror(uint16_t opcode, uint16_t prm1, uint16_t prm2){
    uint32_t result = 0;

    EXTRACT_PARAMS_DR(prm1)

    result = (uint32_t)REG_FILE[prm1];
    if(TESTFLG(C))
        result |= (uint32_t)(1<<8);
    else
        result &= ~((uint32_t)(1<<8));

    /* Set C Flag */
    FLG_TEST((result & (uint32_t)1), C)

    result >>= 1;

    /* Set Flags */
    NEG_TEST_8(result)
    ZERO_TEST_8(result)
    SIGN_TEST()
    FLG_TEST((FLAG(N)^FLAG(C)), V)

    /* Trace info */
    IF_TRACE(){
        printf("Instruction: ROR R%d (0x%.2x) => 0x%.2x\n",
                (int)prm1, REG_FILE[prm1], (int)result);
        print_flags();
    }

    REG_FILE[prm1] = (uint8_t)result;

    return;
}

void asr(uint16_t opcode, uint16_t prm1, uint16_t prm2){
    uint32_t result = 0;

    EXTRACT_PARAMS_DR(prm1)

    result = (uint32_t)REG_FILE[prm1];
    result = (result | ((result & (uint32_t)0x80)<<1));

    /* Set C Flag */
    FLG_TEST((result & (uint32_t)1), C)

    result >>= 1;

    /* Set Flags */
    NEG_TEST_8(result)
    ZERO_TEST_8(result)
    SIGN_TEST()
    FLG_TEST((FLAG(N)^FLAG(C)), V)

    /* Trace info */
    IF_TRACE(){
        printf("Instruction: ASR R%d (0x%.2x) => 0x%.2x\n",
                (int)prm1, REG_FILE[prm1], (int)result);
        print_flags();
    }

    REG_FILE[prm1] = (uint8_t)result;

    return;
}

/* Bit ops */

void sbi(uint16_t opcode, uint16_t prm1, uint16_t prm2){
    uint8_t result = 0;

    EXTRACT_PARAMS_A(prm1)

    result = IO_REGS[prm1];
    result |= (1<<prm2);

    /* Trace info */
    IF_TRACE()
        printf("Instruction: SBI IOR%d = 0x%.4x\n", \
                prm1, IO_REGS[prm2]);

    IO_REGS[prm1] = result;

    return;
}

void cbi(uint16_t opcode, uint16_t prm1, uint16_t prm2){
    uint8_t result = 0;

    /* Close byte gaps */
    EXTRACT_PARAMS_A(prm1)

    result = IO_REGS[prm1];
    result &= ~(1<<prm2);

    /* Trace info */
    IF_TRACE()
        printf("Instruction: CBI IOR%d = 0x%.4x\n", \
                prm1, IO_REGS[prm2]);

    IO_REGS[prm1] = result;

    return;
}

/* Flags */
void bst(uint16_t opcode, uint16_t prm1, uint16_t prm2){

    /* Close byte gaps */
    EXTRACT_PARAMS_DR(prm1)

    if((REG_FILE[prm1] & (1<<prm2)))
        SETFLG(T)
    else
        CLRFLG(T)

    /* Trace info */
    IF_TRACE(){
        printf("Instruction: BST R%d:%d => T\n",
                (int)prm1, (int)prm2);
        print_flags();
    }

    return;
}

void bld(uint16_t opcode, uint16_t prm1, uint16_t prm2){
    uint8_t result = 0;

    /* Close byte gaps */
    EXTRACT_PARAMS_DR(prm1)

    result = REG_FILE[prm1];

    if(TESTFLG(T))
        result |= ((uint8_t)1<<(prm2));
    else
        result &= ~((uint8_t)1<<(prm2));

    IF_TRACE()
        printf("Instruction: BLD R%d (0x%.2x) => 0x%.2x\n",
                (int)prm1, REG_FILE[prm1], result);

    REG_FILE[prm1] = result;

    return;
}

void bset(uint16_t opcode, uint16_t prm1, uint16_t prm2){

    /* Close byte gaps */
    EXTRACT_PARAMS_DR(prm1)

    SETSREG(prm1)

    /* Trace info */
    IF_TRACE(){
        printf("Instruction: BSET SREG:%d\n", (int)prm1);
        print_flags();
    }

    return;
}

void bclr(uint16_t opcode, uint16_t prm1, uint16_t prm2){

    /* Close byte gaps */
    EXTRACT_PARAMS_DR(prm1)

    CLRSREG(prm1)

    /* Trace info */
    IF_TRACE(){
        printf("Instruction: BCLR SREG:%d\n", prm1);
        print_flags();
    }

    return;
}

/* Whole register */
void swap(uint16_t opcode, uint16_t prm1, uint16_t prm2){
    uint8_t result = 0;

    /* Close byte gaps */
    EXTRACT_PARAMS_DR(prm1)

    result = REG_FILE[prm1];
    result = (((result>>4) & 0x0F) | ((result<<4) & 0xF0));

    /* Trace info */
    IF_TRACE(){
        printf("Instruction: SWAP R%d (0x%.2x) => 0x%.2x\n",
                (int)prm1, (int)REG_FILE[prm1], (int)result);
        print_flags();
    }

    REG_FILE[prm1] = result;

    return;
}

/* MCU Control                      */
/* General */
void nop(uint16_t opcode, uint16_t prm1, uint16_t prm2){

    IF_TRACE()
        printf("Instruction: NOP\n");

    return;
}

void slep(uint16_t opcode, uint16_t prm1, uint16_t prm2){

    IF_TRACE()
        printf("Instruction: SLEEP\n");

    return;
}

void wdr(uint16_t opcode, uint16_t prm1, uint16_t prm2){

//    WD_TIMER = 0;

    IF_TRACE()
        printf("Instruction: WDR\n");

    return;
}
