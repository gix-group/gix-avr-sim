/*****************************************************************************
*
* Project:	gix-avr-sim - AVR simulator
*
* File:     regs.c - AVR registers configuration and API file
*
* Copyright (c) 2016, Gil Treibush <tkks085@gmail.com>
* ----------------------------------------------------
* This program is licensed under the MIT license.
*   - A copy of the license is included in a "LICENSE" file in the
*     root directory.
*
*	notes:
*   ------
*
*****************************************************************************/

#include <stdio.h>

#include "regs.h"
#include "flash.h"

/* I/O Registers */
REG8 IO_REGS[64]        = { 0 };

REG8 *ADCSRB    = (REG8*)&IO_REGS[3];   /* : 0x03 */

union REG_ADC *ADC      = (union REG_ADC*)&IO_REGS[4];      /* : 0x04 */

REG8 *ADCSRA    = (REG8*)&IO_REGS[6];   /* : 0x06 */
REG8 *ADMUX     = (REG8*)&IO_REGS[7];   /* : 0x07 */
REG8 *ACSR      = (REG8*)&IO_REGS[8];   /* : 0x08 */
REG8 *USICR     = (REG8*)&IO_REGS[13];  /* : 0x0D */
REG8 *USISR     = (REG8*)&IO_REGS[14];  /* : 0x0E */
REG8 *USIDR     = (REG8*)&IO_REGS[15];  /* : 0x0F */
REG8 *USIBR     = (REG8*)&IO_REGS[16];  /* : 0x10 */
REG8 *GPIOR0    = (REG8*)&IO_REGS[17];  /* : 0x11 */
REG8 *GPIOR1    = (REG8*)&IO_REGS[18];  /* : 0x12 */
REG8 *GPIOR2    = (REG8*)&IO_REGS[19];  /* : 0x13 */
REG8 *DIDR0     = (REG8*)&IO_REGS[20];  /* : 0x14 */
REG8 *PCMSK     = (REG8*)&IO_REGS[21];  /* : 0x15 */
REG8 *PINB      = (REG8*)&IO_REGS[22];  /* : 0x16 */
REG8 *DDRB      = (REG8*)&IO_REGS[23];  /* : 0x17 */
REG8 *PORTB     = (REG8*)&IO_REGS[24];  /* : 0x18 */
REG8 *EECR      = (REG8*)&IO_REGS[28];  /* : 0x1C */
REG8 *EEDR      = (REG8*)&IO_REGS[29];  /* : 0x1D */
REG8 *EEARL     = (REG8*)&IO_REGS[30];  /* : 0x1E */
REG8 *EEARH     = (REG8*)&IO_REGS[31];  /* : 0x1F */
REG8 *PRR       = (REG8*)&IO_REGS[32];  /* : 0x20 */
REG8 *WDTCR     = (REG8*)&IO_REGS[33];  /* : 0x21 */
REG8 *DWDR      = (REG8*)&IO_REGS[34];  /* : 0x22 */
REG8 *DTPS1     = (REG8*)&IO_REGS[35];  /* : 0x23 */
REG8 *DT1B      = (REG8*)&IO_REGS[36];  /* : 0x24 */
REG8 *DT1A      = (REG8*)&IO_REGS[37];  /* : 0x25 */
REG8 *CLKPR     = (REG8*)&IO_REGS[38];  /* : 0x26 */
REG8 *PLLCSR    = (REG8*)&IO_REGS[39];  /* : 0x27 */
REG8 *OCR0B     = (REG8*)&IO_REGS[40];  /* : 0x28 */
REG8 *OCR0A     = (REG8*)&IO_REGS[41];  /* : 0x29 */
REG8 *TCCR0A    = (REG8*)&IO_REGS[42];  /* : 0x2A */
REG8 *OCR1B     = (REG8*)&IO_REGS[43];  /* : 0x2B */
REG8 *GTCCR     = (REG8*)&IO_REGS[44];  /* : 0x2C */
REG8 *OCR1C     = (REG8*)&IO_REGS[45];  /* : 0x2D */
REG8 *OCR1A     = (REG8*)&IO_REGS[46];  /* : 0x2E */
REG8 *TCNT1     = (REG8*)&IO_REGS[47];  /* : 0x2F */
REG8 *TCCR1     = (REG8*)&IO_REGS[48];  /* : 0x30 */
REG8 *OSCCAL    = (REG8*)&IO_REGS[49];  /* : 0x31 */
REG8 *TCNT0     = (REG8*)&IO_REGS[50];  /* : 0x32 */
REG8 *TCCR0B    = (REG8*)&IO_REGS[51];  /* : 0x33 */
REG8 *MCUSR     = (REG8*)&IO_REGS[52];  /* : 0x34 */
REG8 *MCUCR     = (REG8*)&IO_REGS[53];  /* : 0x35 */
REG8 *SPMCSR    = (REG8*)&IO_REGS[55];  /* : 0x37 */
REG8 *TIFR      = (REG8*)&IO_REGS[56];  /* : 0x38 */
REG8 *TIMSK     = (REG8*)&IO_REGS[57];  /* : 0x39 */
REG8 *GIFR      = (REG8*)&IO_REGS[58];  /* : 0x3A */
REG8 *GIMSK     = (REG8*)&IO_REGS[59];  /* : 0x3B */

union REG_SP *M_SP      = (union REG_SP*)&IO_REGS[61];      /* : 0x3D */
union REG_SREG *SREG    = (union REG_SREG*)&IO_REGS[63];    /* : 0x3F */

REG16 PC                = 0;
REG16 INSTREG           = 0;

REG8 REG_FILE[32]       = { 0 };

uint16_t *X_REG = (uint16_t*)&REG_FILE[26];
uint16_t *Y_REG = (uint16_t*)&REG_FILE[28];
uint16_t *Z_REG = (uint16_t*)&REG_FILE[30];

void init_sp(){
    SP = 96;
}

void regs_tick(){
    printf("- regs: Tick\n");
}

/* Funcitons */
void print_flags(){
    printf("Flags - C:%d Z:%d N:%d V:%d S:%d H:%d T:%d I:%d\n\n",
            SREG->flags.C,
            SREG->flags.Z,
            SREG->flags.N,
            SREG->flags.V,
            SREG->flags.S,
            SREG->flags.H,
            SREG->flags.T,
            SREG->flags.I );
}
