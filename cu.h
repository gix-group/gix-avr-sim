/*****************************************************************************
*
* Project:	gix-avr-sim - AVR simulator
*
* File:     cu.h - AVR Control Unit implementation header file
*
* Copyright (c) 2016, Gil Treibush <tkks085@gmail.com>
* ----------------------------------------------------
* This program is licensed under the MIT license.
*   - A copy of the license is included in a "LICENSE" file in the
*     root directory.
*
*	notes:
*   ------
*
*****************************************************************************/

#ifndef __CU__
#define __CU__

#include "regs.h"

#define CUSTATE_INST_READY (1<<0)
#define CUSTATE_INST_32CNT (1<<1)
#define CUSTATE_INST_IGNOR (1<<2)
#define CUSTATE_TRACE_STAT (1<<3)

#define SET_CUSTATE(REG, STATE) (REG) |= (STATE)
#define CLR_CUSTATE(REG, STATE) (REG) &= (~(STATE))

#define INST_READY(REG) ((REG) & (CUSTATE_INST_READY))
#define SET_INST_READY(REG) SET_CUSTATE(REG, CUSTATE_INST_READY)
#define CLEAR_INST_READY(REG) CLR_CUSTATE(REG, CUSTATE_INST_READY)

#define INST_IGNOR(REG) ((REG) & (CUSTATE_INST_IGNOR))
#define CLEAR_INST_IGNOR(REG) CLR_CUSTATE(REG, CUSTATE_INST_IGNOR)
#define SET_INST_IGNOR(REG) SET_CUSTATE(REG, CUSTATE_INST_IGNOR)

#define IF_TRACE_PRINT(REG) if(((REG) & (CUSTATE_TRACE_STAT)))
#define SET_TRACE_PRINT(REG) SET_CUSTATE(REG, CUSTATE_TRACE_STAT)

/* API functions */
void init_cu_state();
void cu_tick();

#endif
