/*****************************************************************************
*
* Project:	gix-avr-sim - AVR simulator
*
* File:     parse_hex.h - AVR ".hex"-files parser header file.
*
* Copyright (c) 2016, Gil Treibush <tkks085@gmail.com> 
* ----------------------------------------------------
* This program is licensed under the MIT license.
*   - A copy of the license is included in a "LICENSE" file in the
*     root directory.
*
*	notes:
*       ------
*
*****************************************************************************/

#ifndef __PARSE_HEX__
#define __PARSE_HEX__

#include"mem.h"
#include"flash.h"

#define LINE_BUFF_MAX 255
#define CNVRT_BUFF_SZ 8

/* Record type codes */
#define RTYPE_DATA "00"
#define RTYPE_EOFL "01"
#define RTYPE_ESAD "02"
#define RTYPE_SSAD "03"
#define RTYPE_ELAD "04"
#define RTYPE_SLAD "05"

/* Return values */
typedef enum {
    next=0,
    stop_err,
    stop_eof
} parse_ret;

/* hex file format:
 *
 *    +-+--+----+--+            +--+
 *    |:|bc|addr|rt|... data ...|cs|
 *    +-+--+----+--+            +--+
 *      1  3    7  9            x  x+2
 *
 *    :    - A single colon char
 *    bc   - Byte count
 *    addr - Address
 *    rt   - Record type
 *    cs   - Data checksum
 *
 */

/* Line header template */
struct hex_header {
    unsigned char col;
    unsigned char bcnt[2];
    unsigned char addr[4];
    unsigned char rtyp[2];

} __attribute__((packed)) ;

struct hex_footer {
    unsigned char crc[2];
    char endl[2];

} __attribute__((packed)) ;

/* Single line of hex data */
struct hex_line {
    struct hex_header   header;
    uint8_t             size;
    uint16_t            addr;
    unsigned char       data[LINE_BUFF_MAX];
    struct hex_footer   footer;
    uint8_t             crc;
};

/* API functions */
int hex_map_mem(int, struct mem*);

#endif
