/*****************************************************************************
*
* Project:	gix-avr-sim - AVR simulator
*
* File:     sram.h - SRAM memory header file.
*
* Copyright (c) 2016, Gil Treibush <tkks085@gmail.com>
* ----------------------------------------------------
* This program is licensed under the MIT license.
*   - A copy of the license is included in a "LICENSE" file in the
*     root directory.
*
*	notes:
*   ------
*
*****************************************************************************/

#ifndef __SRAM__
#define __SRAM__

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

#include "mem.h"

/* External variables */
extern struct mem *SRAM;

/* API functions */
int  sram_init();
void sram_free();

int         sram_word_ptr(addr_t, uint8_t**);
uint8_t     sram_read_word(addr_t, int*);
uint16_t    sram_read_dword(addr_t, int*);

int         sram_write_word(addr_t, uint8_t);
int         sram_write_dword(addr_t, uint16_t);

void        sram_info();

void        sram_tick();

#endif
