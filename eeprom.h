/*****************************************************************************
*
* Project:	gix-avr-sim - AVR simulator
*
* File:     eeprom.h - AVR EEPROM configuration and API header file
*
* Copyright (c) 2016, Gil Treibush <tkks085@gmail.com> 
* ----------------------------------------------------
* This program is licensed under the MIT license.
*   - A copy of the license is included in a "LICENSE" file in the
*     root directory.
*
*	notes:
*   ------
*
*****************************************************************************/

#ifndef __EEPROM__
#define __EEPROM__

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

#include "mem.h"

/* External variables */
extern struct mem *EEPROM;

/* API functions */
int         eeprom_init();
void        eeprom_free();

int         eeprom_word_ptr(addr_t, uint8_t**);
uint8_t     eeprom_read_word(addr_t, int*);

void        eeprom_info();

void        eeprom_tick();

#endif
