/*****************************************************************************
*
* Project:	gix-avr-sim - AVR simulator
*
* File:     sram.c - SRAM memory implementation.
*
* Copyright (c) 2016, Gil Treibush <tkks085@gmail.com>
* ----------------------------------------------------
* This program is licensed under the MIT license.
*   - A copy of the license is included in a "LICENSE" file in the
*     root directory.
*
*	notes:
*       ------
*
*****************************************************************************/

#include"sram.h"
#include"regs.h"

/* Global SRAM pointer */
struct mem *SRAM = NULL;

/*** API functions ***/

int sram_init(){
    uint8_t *sram_mem;

    if(SRAM)
        return -1;

    sram_mem = (uint8_t*)malloc(sizeof(uint8_t)*256);
    if(!sram_mem)
        return -1;

    SRAM = mem_init(WRDWDTH8);
    if(!SRAM)
        goto fail_sram_out;

    if(mem_add_sect(SRAM, (addr_t)0, (addr_t)31, REG_FILE)<0)
        goto fail_sect_out;

    if(mem_add_sect(SRAM, (addr_t)32, (addr_t)95, IO_REGS)<0)
        goto fail_sect_out;

    if(mem_add_sect(SRAM, (addr_t)96, (addr_t)346, sram_mem)<0)
        goto fail_sect_out;

    return 0;

fail_sect_out:
    mem_free(SRAM);
fail_sram_out:
    free(sram_mem);
    return -1;
}

void sram_free(){
    struct mem_sect* sect;

    sect = mem_get_sect(SRAM, (addr_t)96);
    if(sect)
        free(sect->mem);

    mem_free(SRAM);
}

int sram_word_ptr(addr_t addr, uint8_t **sram_word){
    struct mem_sect* sect;
    addr_t wrd_addr;

    sect = mem_get_sect(SRAM, addr);
    if(!sect){
        *sram_word = NULL;
        return 0;
    }

    wrd_addr = addr - sect->start_addr;
    *sram_word = &((uint8_t*)sect->mem)[wrd_addr];

    return (int)sect->end_addr - addr;
}

uint8_t sram_read_word(addr_t addr, int *err){
    union mem_word mem_word;

    *err = 0;

    if(mem_read_word(SRAM, addr, &mem_word) == 0)
        return (mem_word.WRD8);

    *err = -1;
    return 0;
}

uint16_t sram_read_dword(addr_t addr, int *err){
    uint16_t sram_dword;
    int read_status;

    *err = 0;

    sram_dword = sram_read_word(addr, &read_status);
    if(read_status<0){
        *err = -1;
        return 0;
    }

    sram_dword = (sram_dword<<8) & 0xFF00;
    sram_dword |= sram_read_word(addr+1, &read_status);

    if(read_status<0){
        *err = -1;
        return 0;
    }

    return sram_dword;
}

int sram_write_word(addr_t addr, uint8_t sram_word){
    union mem_word sram_mem_word;
    sram_mem_word.WRD8 = sram_word;

    if(mem_write_word(SRAM, addr, &sram_mem_word) == 0)
        return 0;

    return -1;
}

int sram_write_dword(addr_t addr, uint16_t sram_word){
    struct mem_sect* sect;

    if(mem_get_sect(SRAM, addr) && mem_get_sect(SRAM, addr+1)){
        sram_write_word(addr, (uint8_t)((sram_word>>8) & 0x00FF));
        sram_write_word(addr+1, (uint8_t)(sram_word & 0x00FF));
        return 0;
    }

    return -1;
}

void sram_info(){
    mem_info(SRAM);
}

void sram_tick(){
    printf("- sram: Tick\n");
}
