/*****************************************************************************
*
* Project:	gix-avr-sim - AVR simulator
*
* File:     alu.h - AVR ALU state header file
*
* Copyright (c) 2016, Gil Treibush <tkks085@gmail.com> 
* ----------------------------------------------------
* This program is licensed under the MIT license.
*   - A copy of the license is included in a "LICENSE" file in the
*     root directory.
*
*	notes:
*       ------
*
*****************************************************************************/

#ifndef __CMD__
#define __CMD__

typedef struct {
    uint8_t val1;
    uint8_t val2;
} ALU;

extern ALU ALU;

#endif
