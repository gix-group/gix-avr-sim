/*****************************************************************************
*
* Project:	gix-avr-sim - AVR simulator
*
* File:     clocks.h - AVR system timers configuration and API header file
*
* Copyright (c) 2016, Gil Treibush <tkks085@gmail.com> 
* ----------------------------------------------------
* This program is licensed under the MIT license.
*   - A copy of the license is included in a "LICENSE" file in the
*     root directory.
*
*	notes:
*   ------
*
*****************************************************************************/

#ifndef __CLOCKS__
#define __CLOCKS__

#define OPFREQ8MHZ (freq)8000000

/* Frequency definitions */
typedef unsigned long int freq;
typedef unsigned long int timer;

/* Possible clock soruces */
typedef enum {
    clk_external,
    clk_hf_ppl,
    clk_calib_osc,
    clk_internal_osc,
    clk_lf_crystal_osc,
    clk_crystal_osc
} clk_src;

#define TICKS_NUM 4

struct clk {
    timer val;
    void (*ticks[TICKS_NUM])();
    clk_src src;
};

/* All AVR clocks */
struct avr_clks {
    struct clk cpu_clk;
    struct clk io_clk;
    struct clk flash_clk;
    struct clk adc_clk;
};

union clocks {
    struct avr_clks clks;
    struct clk clks_arr[4];
};

/* API functions */
void init_clocks();
void start_clocks();

#endif
