/*****************************************************************************
*
* Project:	gix-avr-sim - AVR simulator
*
* File:     isa.h - AVR ISA configuration header file
*
* Copyright (c) 2016, Gil Treibush <tkks085@gmail.com>
* ----------------------------------------------------
* This program is licensed under the MIT license.
*   - A copy of the license is included in a "LICENSE" file in the
*     root directory.
*
*	notes:
*   ------
*
*****************************************************************************/

#ifndef __CMD__
#define __CMD__

#include <stdint.h>

#define MSK16_BYTE1 0x000F
#define MSK16_BYTE2 0x00F0
#define MSK16_BYTE3 0x0F00
#define MSK16_BYTE4 0xF000

#define MSK32_BYTE1 0x0000000F
#define MSK32_BYTE2 0x000000F0
#define MSK32_BYTE3 0x00000F00
#define MSK32_BYTE4 0x0000F000
#define MSK32_BYTE5 0x000F0000
#define MSK32_BYTE6 0x00F00000
#define MSK32_BYTE7 0x0F000000
#define MSK32_BYTE8 0xF0000000

#define AVRCMD2(SIZE) struct avr_cmd_ ##SIZE {          \
            uint ##SIZE ##_t cmd_raw;                   \
            uint ##SIZE ##_t oc_mask;                   \
            uint ##SIZE ##_t p1_mask;                   \
            uint ##SIZE ##_t p2_mask;                   \
            void (*exec)(   uint ##SIZE ##_t op_code,   \
                            uint ##SIZE ##_t p1,        \
                            uint ##SIZE ##_t p2);       \
            }

typedef union {
    AVRCMD2(16) cmd_16;
    AVRCMD2(32) cmd_32;
} avr_cmd;

#define CMDARR(NAME) avr_cmd NAME[]

extern CMDARR(CMDS);

#define CMD16(OPCODE, OCMASK, P1MASK, P2MASK, EXEC) {   \
                .cmd_16 = {                             \
                    .cmd_raw = OPCODE,                  \
                    .oc_mask = OCMASK,                  \
                    .p1_mask = P1MASK,                  \
                    .p2_mask = P2MASK,                  \
                    .exec = EXEC                        \
                }                                       \
        }

#define CMD32(OPCODE, OCMASK, P1MASK, P2MASK, EXEC) {   \
                .cmd_32 = {                             \
                    .cmd_raw = OPCODE,                  \
                    .oc_mask = OCMASK,                  \
                    .p1_mask = P1MASK,                  \
                    .p2_mask = P2MASK,                  \
                    .exec = EXEC                        \
                }                                       \
        }

/* "cmds" array access macros */

#define cmd16_raw(NUM) cmds[ ##NUM ##].cmd_16.cmd_raw
#define cmd16_ocmsk(NUM) cmds[ ##NUM ##].cmd_16.oc_mask
#define cmd16_p1msk(NUM) cmds[ ##NUM ##].cmd_16.p1_mask
#define cmd16_p2msk(NUM) cmds[ ##NUM ##].cmd_16.p2_mask
#define cmd16_exec(NUM) cmds[ ##NUM ##].cmd_16.exec

#define cmd32_raw(NUM) cmds[ ##NUM ##].cmd_32.cmd_raw
#define cmd32_ocmsk(NUM) cmds[ ##NUM ##].cmd_32.oc_mask
#define cmd32_p1msk(NUM) cmds[ ##NUM ##].cmd_32.p1_mask
#define cmd32_p2msk(NUM) cmds[ ##NUM ##].cmd_32.p2_mask
#define cmd32_exec(NUM) cmds[ ##NUM ##].cmd_32.exec

/**********************************************/
/***** Instructions function signatures *******/
/**********************************************/

/*** Arithmetic/Logic Instructions: Arithmetic  ***/

/* Add */
void add(uint16_t, uint16_t, uint16_t);
void adc(uint16_t, uint16_t, uint16_t);
void adiw(uint16_t, uint16_t, uint16_t);

/* Subtract */
void sub(uint16_t, uint16_t, uint16_t);
void sbc(uint16_t, uint16_t, uint16_t);
void subi(uint16_t, uint16_t, uint16_t);
void sbci(uint16_t, uint16_t, uint16_t);
void sbiw(uint16_t, uint16_t, uint16_t);

#ifdef MUL_INST 
/* Multiply */
void mul(uint16_t, uint16_t, uint16_t);
void muls(uint16_t, uint16_t, uint16_t);
void mulsu(uint16_t, uint16_t, uint16_t);
void fmul(uint16_t, uint16_t, uint16_t);
void fmuls(uint16_t, uint16_t, uint16_t);
void fmulsu(uint16_t, uint16_t, uint16_t);
#endif

/* ++, -- */
void inc(uint16_t, uint16_t, uint16_t);
void dec(uint16_t, uint16_t, uint16_t);

/*** Arithmetic/Logic Instructions: Logic ***/

/* Basic logic ops */
void and(uint16_t, uint16_t, uint16_t);
void andi(uint16_t, uint16_t, uint16_t);
void or(uint16_t, uint16_t, uint16_t);
void ori(uint16_t, uint16_t, uint16_t);
void eor(uint16_t, uint16_t, uint16_t);

/* Two's Complement */
void com(uint16_t, uint16_t, uint16_t);
void neg(uint16_t, uint16_t, uint16_t);

/* Encryption */
void des(uint16_t, uint16_t, uint16_t);

/*** Branch Instructions              ***/

/* Jump */
void rjmp(uint16_t, uint16_t, uint16_t);
void ijmp(uint16_t, uint16_t, uint16_t);
void eijmp(uint16_t, uint16_t, uint16_t);
void jmp(uint32_t, uint32_t, uint32_t);

/* Calls */
void rcall(uint16_t, uint16_t, uint16_t);
void icall(uint16_t, uint16_t, uint16_t);
void eicall(uint16_t, uint16_t, uint16_t);
void call(uint32_t, uint32_t, uint32_t);
void ret(uint16_t, uint16_t, uint16_t);
void reti(uint16_t, uint16_t, uint16_t);

/* Branch */
void brbc(uint16_t, uint16_t, uint16_t);
void brbs(uint16_t, uint16_t, uint16_t);

/* Skip */
void cpse(uint16_t, uint16_t, uint16_t);
void sbrc(uint16_t, uint16_t, uint16_t);
void sbrs(uint16_t, uint16_t, uint16_t);
void sbic(uint16_t, uint16_t, uint16_t);
void sbis(uint16_t, uint16_t, uint16_t);

/* Compare */
void cp(uint16_t, uint16_t, uint16_t);
void cpc(uint16_t, uint16_t, uint16_t);
void cpi(uint16_t, uint16_t, uint16_t);

/*** Data Transfer Instructions       ***/

/* Move */
void mov(uint16_t, uint16_t, uint16_t);
void movw(uint16_t, uint16_t, uint16_t);

/* Load */
void ldi(uint16_t, uint16_t, uint16_t);
#ifdef LDST_16
void lds(uint16_t, uint16_t, uint16_t);
#else
void lds(uint32_t, uint32_t, uint32_t);
#endif
void ldx(uint16_t, uint16_t, uint16_t);
void ldy(uint16_t, uint16_t, uint16_t);
void ldz(uint16_t, uint16_t, uint16_t);

/* Store */
#ifdef LDST_16
void sts(uint16_t, uint16_t, uint16_t);
#else
void sts(uint32_t, uint32_t, uint32_t);
#endif
void stx(uint16_t, uint16_t, uint16_t);
void sty(uint16_t, uint16_t, uint16_t);
void stz(uint16_t, uint16_t, uint16_t);

/* Prog mem */
void lpm(uint16_t, uint16_t, uint16_t);
void elpm(uint16_t, uint16_t, uint16_t);
void spm(uint16_t, uint16_t, uint16_t);
void spm2(uint16_t, uint16_t, uint16_t);

/* Data mem */
void las(uint16_t, uint16_t, uint16_t);
void lac(uint16_t, uint16_t, uint16_t);
void lat(uint16_t, uint16_t, uint16_t);
#ifdef XCHN
void xch(uint16_t, uint16_t, uint16_t);
#endif

/* I/O */
void out(uint16_t, uint16_t, uint16_t);
void in(uint16_t, uint16_t, uint16_t);

/* Stack ops */
void push(uint16_t, uint16_t, uint16_t);
void pop(uint16_t, uint16_t, uint16_t);

/*** Bit and Bit-test                 ***/

/* Shift/Rotate */
void lsr(uint16_t, uint16_t, uint16_t);
void ror(uint16_t, uint16_t, uint16_t);
void asr(uint16_t, uint16_t, uint16_t);

/* Bit ops */
void sbi(uint16_t, uint16_t, uint16_t);
void cbi(uint16_t, uint16_t, uint16_t);

/* Flags */
void bst(uint16_t, uint16_t, uint16_t);
void bld(uint16_t, uint16_t, uint16_t);
void bset(uint16_t, uint16_t, uint16_t);
void bclr(uint16_t, uint16_t, uint16_t);

/* Whole register */
void swap(uint16_t, uint16_t, uint16_t);

/*** MCU Control                      ***/

/* General */
void nop(uint16_t, uint16_t, uint16_t);
void slep(uint16_t, uint16_t, uint16_t);
void wdr(uint16_t, uint16_t, uint16_t);

#endif
