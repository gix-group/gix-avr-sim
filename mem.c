/*****************************************************************************
*
* Project:	gix-avr-sim - AVR simulator
*
* File:     mem.c - memory base implementation file.
*
* Copyright (c) 2016, Gil Treibush <tkks085@gmail.com> 
* ----------------------------------------------------
* This program is licensed under the MIT license.
*   - A copy of the license is included in a "LICENSE" file in the
*     root directory.
*
*	notes:
*   ------
*
*****************************************************************************/

#include <stdlib.h>
#include <stdio.h>

#include "mem.h"

/*** API functions ***/

/* mem_init
 * Initialize memory structure.
 */
struct mem *mem_init(wrd_wdth wrdw){
    struct mem *new_mem;

    new_mem = (struct mem*)malloc(sizeof(struct mem));
    if(!new_mem)
        return NULL;

    new_mem->SOM    = 0;
    new_mem->EOM    = 0;
    new_mem->wrdw   = wrdw;

    SLIST_INIT(&new_mem->sects);

    return new_mem;
}

/* mem_free
 * Release memory structure.
 */
void mem_free(struct mem* mem){
    struct mem_sect *ptr_sect;

    if(!mem)
        return;

    while (!SLIST_EMPTY(&mem->sects)) {
            ptr_sect = SLIST_FIRST(&mem->sects);
            SLIST_REMOVE_HEAD(&mem->sects, sect_lst);
            free(ptr_sect);
    }

    free(mem);
    mem = NULL;
}

/* mem_add_sect
 * Add new memory section.
 */
int mem_add_sect(struct mem *mem, addr_t start_addr, addr_t end_addr, void *mem_ref) {
    struct mem_sect *new_sect, *ptr_sect, *last_sect;

    if(start_addr > end_addr)
        return -MEMERR_ADDRS;

    if(!mem_ref)
        return -MEMERR_PARAM;

    SLIST_FOREACH(ptr_sect, &mem->sects, sect_lst){
        if(!(ptr_sect->start_addr > end_addr || ptr_sect->end_addr < start_addr))
            return -MEMERR_ADDRS;

        last_sect = ptr_sect;
    }

    new_sect = (struct mem_sect*)malloc(sizeof(struct mem_sect));
    if(!new_sect)
        return -MEMERR_ALLOC;

    if(SLIST_EMPTY(&mem->sects))
        SLIST_INSERT_HEAD(&mem->sects, new_sect, sect_lst);
    else
        SLIST_INSERT_AFTER(last_sect, new_sect, sect_lst);

    new_sect->start_addr    = start_addr;
    new_sect->end_addr      = end_addr;
    new_sect->mem           = mem_ref;

    return 0;
}

/* mem_get_sect
 * Returns an existing memory section which containes the 
 * given address.
 */
struct mem_sect *mem_get_sect(struct mem *mem, addr_t addr){
    struct mem_sect *ptr_sect;

    SLIST_FOREACH(ptr_sect, &mem->sects, sect_lst){
        if(ptr_sect->end_addr >= addr && ptr_sect->start_addr <= addr)
            return ptr_sect;
    }

    return NULL;
}

/* mem_read_word
 * Reads a single word from memory, from a given address.
 */
int mem_read_word(struct mem *mem, addr_t addr, union mem_word *mem_word){
    struct mem_sect *mem_sect;
    addr_t wrd_addr;

    mem_sect = mem_get_sect(mem, addr);
    if(!mem_sect){
        return -MEMERR_ADDRS;
    }

    wrd_addr = addr - mem_sect->start_addr;

    switch(mem->wrdw){
        case WRDWDTH8:
            mem_word->WRD8 = ((uint8_t*)mem_sect->mem)[wrd_addr];
            return 0;

        case WRDWDTH16:
            mem_word->WRD16 = ((uint16_t*)mem_sect->mem)[wrd_addr];
            return 0;

        case WRDWDTH32:
            mem_word->WRD32 = ((uint32_t*)mem_sect->mem)[wrd_addr];
            return 0;
    }

    return -MEMERR_UNDEF;
}

/* mem_write_word
 * Writes a single word into memory, at a given address.
 */
int mem_write_word(struct mem* mem, addr_t addr, union mem_word *mem_word){
    struct mem_sect *mem_sect;
    addr_t wrd_addr;

    mem_sect = mem_get_sect(mem, addr);
    if(!mem_sect){
        return -MEMERR_ADDRS;
    }

    wrd_addr = addr - mem_sect->start_addr;

    switch(mem->wrdw){
        case WRDWDTH8:
            ((uint8_t*)mem_sect->mem)[wrd_addr] = mem_word->WRD8;
            return 0;

        case WRDWDTH16:
            ((uint16_t*)mem_sect->mem)[wrd_addr] = mem_word->WRD16;
            return 0;

        case WRDWDTH32:
            ((uint32_t*)mem_sect->mem)[wrd_addr] = mem_word->WRD32;
            return 0;
    }

    return -MEMERR_UNDEF;

}

/* mem_info
 * Prints out a memory sections for Debug.
 */
void mem_info(struct mem *mem){
    struct mem_sect *ptr_sect;

    printf("Memory sections:\n");
    SLIST_FOREACH(ptr_sect, &mem->sects, sect_lst){
        printf("\tSection: %3u - %3u\n",
                ptr_sect->start_addr,
                ptr_sect->end_addr);
    }

    return;
}
