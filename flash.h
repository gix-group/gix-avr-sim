/*****************************************************************************
*
* Project:	gix-avr-sim - AVR simulator
*
* File:     flash.h - Flash memory implementation header file.
*
* Copyright (c) 2016, Gil Treibush <tkks085@gmail.com>
* ----------------------------------------------------
* This program is licensed under the MIT license.
*   - A copy of the license is included in a "LICENSE" file in the
*     root directory.
*
*	notes:
*   ------
*
*****************************************************************************/

#ifndef __FLASH__
#define __FLASH__

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

#include "mem.h"

/* External variables */
extern struct mem *FLASH;

/* API functions */
int         flash_init();
void        flash_free();

int         flash_word_ptr(addr_t, uint16_t**);
uint16_t    flash_read_word(addr_t, int*);

void        flash_info();

void        flash_tick();

#endif
