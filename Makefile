
TARGET_NAME = gix-avr-sim
#TEST_TARGET_NAME = gix-avr-sim-test

ARCH_DIR = arch

#SRCS = mmu.c cu.c flash.c parse_hex.c regs.c timers.c mem.c
SRCS = mcu.c mem.c parse_hex.c flash.c sram.c regs.c clocks.c cu.c eeprom.c
#TEST_SRCS = utest.c parse_hex.c  mem.c flash.c sram.c eeprom.c regs.c timers.c cu.c
ARCH_SRCS = $(wildcard $(ARCH_DIR)/*.c)

OBJS = $(SRCS:.c=.o)
#TEST_OBJS = $(TEST_SRCS:.c=.o)
ARCH_OBJS = $(ARCH_SRCS:.c=.o)

.PHONY: all utest clean

all: $(TARGET_NAME)

#test: $(TEST_TARGET_NAME)

$(TARGET_NAME): $(OBJS)
	$(CC) -o $(@) $(?)

#$(TEST_TARGET_NAME): $(TEST_OBJS)
#	$(CC) -o $(@) $(?)

attiny85: ;

attiny45: ;

attiny25: $(OBJS) $(ARCH_DIR)/attiny25.o
	$(CC) -o $(@)-sim $(?)

$(OBJS): $(SRCS)

#$(TEST_OBJS): $(TEST_SRCS)

$(ARCH_OBJS): $(ARCH_SRCS)

clean:
	rm -f $(OBJS)
	rm -f $(ARCH_OBJS)
	rm -f $(TARGET_NAME)
