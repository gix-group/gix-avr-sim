/*****************************************************************************
*
* Project:	gix-avr-sim - AVR simulator
*
* File:     eeprom.c - EEPROM memory implementation.
*
* Copyright (c) 2016, Gil Treibush <tkks085@gmail.com> 
* ----------------------------------------------------
* This program is licensed under the MIT license.
*   - A copy of the license is included in a "LICENSE" file in the
*     root directory.
*
*	notes:
*   ------
*
*****************************************************************************/

#include"eeprom.h"

/* Global eeprom pointer */
struct mem *EEPROM = NULL;

/* API functions */
int eeprom_init(){
    uint8_t *eeprom_mem;

    if(EEPROM)
        return -1;

    eeprom_mem = (uint8_t*)malloc(sizeof(uint8_t)*256);
    if(!eeprom_mem)
        return -1;

    EEPROM = mem_init(WRDWDTH8);
    if(!EEPROM)
        goto fail_eeprom_out;

    if(mem_add_sect(EEPROM, (addr_t)0, (addr_t)256, eeprom_mem)<0)
        goto fail_sect_out;

    return 0;

fail_sect_out:
    mem_free(EEPROM);
fail_eeprom_out:
    free(eeprom_mem);
    return -1;
}

void eeprom_free(){
    struct mem_sect* sect;

    sect = mem_get_sect(EEPROM, (addr_t)0);
    if(sect)
        free(sect->mem);

    mem_free(EEPROM);
}

uint8_t eeprom_read_word(addr_t addr, int *err){
    union mem_word mem_word;

    *err = 0;

    if(mem_read_word(EEPROM, addr, &mem_word) == 0)
        return (mem_word.WRD8);

    *err = -1;
    return 0;
}

int eeprom_write_word(addr_t addr, uint8_t eeprom_word){
    union mem_word mem_word;
    mem_word.WRD8 = eeprom_word;

    if(mem_write_word(EEPROM, addr, &mem_word) == 0)
        return 0;

    return -1;
}

void eeprom_info(){
    mem_info(EEPROM);
}

void eeprom_tick(){
    printf("- eeprom: Tick\n");
}
