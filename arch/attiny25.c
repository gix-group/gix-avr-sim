/*****************************************************************************
*
* Project:	gix-avr-sim - AVR simulator
*
* File:     attiny25.c - AVR 
*
* Copyright (c) 2016, Gil Treibush <tkks085@gmail.com> 
* ----------------------------------------------------
* This program is licensed under the MIT license.
*   - A copy of the license is included in a "LICENSE" file in the
*     root directory.
*
*	notes:
*       ------
*
*****************************************************************************/

#include"attiny25.h"
