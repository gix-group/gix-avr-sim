/*****************************************************************************
*
* Project:	gix-avr-sim - AVR simulator
*
* File:     attiny25.h - AVR 
*
* Copyright (c) 2016, Gil Treibush <tkks085@gmail.com> 
* ----------------------------------------------------
* This program is licensed under the MIT license.
*   - A copy of the license is included in a "LICENSE" file in the
*     root directory.
*
*	notes:
*       ------
*
*****************************************************************************/

#ifndef __REGS__
#define __REGS__

typedef struct {
    unsigned int usecs;
    unsigned int secs;
} AVR_TIMER;

typedef void (*tick_cb)();

struct cycle_ops {
    int ops_cnt;
    tick_cb *cbs;
};

extern struct cycle_ops *CY_OPS;

typedef unsigned long counter;

/* External variables API */
extern counter cycles;
extern AVR_TIMER wall_clock;

/* API functions */
int init_cycle_ops(tick_cb*);
void TICK();

#endif
