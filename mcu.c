/*****************************************************************************
*
* Project:	gix-avr-sim - AVR simulator
*
* File:     mcu.c - AVR MCU (top level) configuration file.
*
* Copyright (c) 2016, Gil Treibush <tkks085@gmail.com> 
* ----------------------------------------------------
* This program is licensed under the MIT license.
*   - A copy of the license is included in a "LICENSE" file in the
*     root directory.
*
*	notes:
*   ------
*
*****************************************************************************/

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <getopt.h>

#include "mcu.h"
#include "cu.h"

/* MCU Memory API */
#include "flash.h"
#include "sram.h"
#include "eeprom.h"

#include "regs.h"
#include "clocks.h"
#include "parse_hex.h"

int memory_program(char* file_flash, char* file_eeprom){
    int flash_fd, eeprom_fd;

    if(file_flash){
        flash_fd = open(file_flash, O_RDONLY);
        if(flash_fd > -1){
            printf("Program instruction file: %s\n", file_flash);
            hex_map_mem(flash_fd, FLASH);
            close(flash_fd);
        } else {
            fprintf(stderr, "ERROR: Could not open Flash file %s\n", file_flash);
            return -1;
        }
    }

    if(file_eeprom){
        eeprom_fd = open(file_eeprom, O_RDONLY);
        if(eeprom_fd > -1){
            printf("EEPROM memory file: %s\n", file_eeprom);
            hex_map_mem(eeprom_fd, EEPROM);
            close(eeprom_fd);
        } else {
            fprintf(stderr, "ERROR: Could not open EEPROM file %s\n", file_eeprom);
            return -1;
        }
    }

    return 0;
}

static int memory_init(int flash_size, int sram_size, int eeprom_size){
    int ret;

    if(flash_size)
        ret = flash_init();
        if(ret < 0)
            goto mem_out;

    if(eeprom_size)
        ret = eeprom_init();
        if(ret < 0)
            goto mem_out;

    ret = sram_init();

mem_out:
    return ret;
}

static void memory_free(){
        flash_free();
        sram_free();
        eeprom_free();
}

static void usage() {
    printf( "usage: gix-avr-sim [OPTIONS]\n"
            "Gix project AVR microcontroller simulator\n"
            "options:\n"
            "\t-f, --flash_file     this is the main program file\n"
            "\t-e, --eeprom_file    this is the eeprom memory file\n"
            "\t-t, --tui            text-based user interface\n"
            "\t    --help           display this help menue\n");
}

int main(int argc, char** argv){
    int opt;
    int opt_ind;
    int ret = 0;

    static int tui_flag = 0;

    char *file_flash    = NULL;
    char *file_eeprom   = NULL;

    static struct option long_options[] =
    {
        {"tui",         no_argument,       &tui_flag,   1},
        {"flash_file",  required_argument, NULL,        'f'},
        {"eeprom_file", required_argument, 0,           'e'},
        {"help",        required_argument, 0,           'h'},
        {0, 0, 0, 0}
    };

    while((opt = getopt_long(argc, argv, "tf:e:h", long_options, &opt_ind))>0){
        switch(opt){

        case 'f':
            file_flash = optarg;
            break;

        case 'e':
            file_eeprom = optarg;
            break;

        case 't':
            // Set TUI mode
            break;

        case 'h':
            usage();
            return 0;

        default:
            /* Default Unknown Argument error produced by optarg */
            return -1;
        }
    }

    printf("GIX Project AVR Simulator %s\n", VERSION);

    /* Initialize MCU memory */
    ret = memory_init(512, 256, 256);
    if(ret<0){
        fprintf(stderr, "Could not finish run. Leaving.\n");
        goto fail_out;
    }

    /* Populate program memory */
    ret = memory_program(file_flash, file_eeprom);
    if(ret<0){
        fprintf(stderr, "Could not finish run. Leaving.\n");
        goto fail_out;
    }

    init_cu_state();

    init_clocks();

    init_sp();

    start_clocks();

    memory_free();

    return 0;

fail_out:
    return -1;
}
