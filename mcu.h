/*****************************************************************************
*
* Project:	gix-avr-sim - AVR simulator
*
* File:     mcu.h - AVR MCU (top level) configuration header file.
*
* Copyright (c) 2016, Gil Treibush <tkks085@gmail.com> 
* ----------------------------------------------------
* This program is licensed under the MIT license.
*   - A copy of the license is included in a "LICENSE" file in the
*     root directory.
*
*	notes:
*   ------
*
*****************************************************************************/

#ifndef __MMU__
#define __MMY__

#define VERSION "0.0.1"

#define GIX_INFO(...) fprintf(stdout, "INFO: ", ## __VA_ARGS__)
#define GIX_WARN(...) fprintf(stdout, "WARNING: ", ## __VA_ARGS__)
#define GIX_ERR(...)  fprintf(stderr, "ERROR: ", ## __VA_ARGS__)

#endif
