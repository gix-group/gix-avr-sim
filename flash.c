/*****************************************************************************
*
* Project:	gix-avr-sim - AVR simulator
*
* File:     flash.c - Flash memory implementation.
*
* Copyright (c) 2016, Gil Treibush <tkks085@gmail.com> 
* ----------------------------------------------------
* This program is licensed under the MIT license.
*   - A copy of the license is included in a "LICENSE" file in the
*     root directory.
*
*	notes:
*   ------
*
*****************************************************************************/

#include"flash.h"

/* Global Flash pointer */
struct mem *FLASH = NULL;

/* API functions */

/* flash_init
 * 
 */
int flash_init(){
    uint16_t *flash_mem;

    if(FLASH)
        return -1;

    flash_mem = (uint16_t*)malloc(sizeof(uint16_t)*256);
    if(!flash_mem)
        return -1;

    FLASH = mem_init(WRDWDTH16);
    if(!FLASH)
        goto fail_flash_out;

    if(mem_add_sect(FLASH, (addr_t)0, (addr_t)256, flash_mem)<0)
        goto fail_mem_out;

    return 0;

fail_mem_out:
    mem_free(FLASH);

fail_flash_out:
    free(flash_mem);
    return -1;
}

/* flash_init
 * 
 */
void flash_free(){
    struct mem_sect* sect;

    sect = mem_get_sect(FLASH, (addr_t)0);
    if(sect)
        free(sect->mem);

    mem_free(FLASH);
}

/* flash_init
 * 
 */
int flash_word_ptr(addr_t addr, uint16_t **flash_word){
    struct mem_sect* sect;
    addr_t wrd_addr;

    sect = mem_get_sect(FLASH, addr);
    if(!sect){
        *flash_word = NULL;
        return 0;
    }

    wrd_addr = addr - sect->start_addr;
    *flash_word = &((uint16_t*)sect->mem)[wrd_addr];

    return (int)sect->end_addr - addr;
}

/* flash_init
 * 
 */
uint16_t flash_read_word(addr_t addr, int *err){
    union mem_word mem_word;

    *err = 0;

    if(mem_read_word(FLASH, addr, &mem_word) == 0)
        return (mem_word.WRD16);

    *err = -1;
    return 0;
}

/* flash_init
 * 
 */
void flash_info(){
    mem_info(FLASH);
}

/* flash_init
 * 
 */
void flash_tick(){
    printf("- flash: Tick\n");
}
