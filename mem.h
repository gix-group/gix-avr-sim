/*****************************************************************************
*
* Project:	gix-avr-sim - AVR simulator
*
* File:     mem.h - memory base header file.
*
* Copyright (c) 2016, Gil Treibush <tkks085@gmail.com>
* ----------------------------------------------------
* This program is licensed under the MIT license.
*   - A copy of the license is included in a "LICENSE" file in the
*     root directory.
*
*	notes:
*   ------
*
*****************************************************************************/

#ifndef __MEM__
#define __MEM__

#include <stdint.h>
#include <sys/queue.h>

typedef unsigned int addr_t;

/* Memory error codes */
#define MEMERR_ALLOC 1
#define MEMERR_ADDRS 2
#define MEMERR_PARAM 3
#define MEMERR_UNDEF 4

/* Word width enum */
typedef enum { 
    WRDWDTH8,
    WRDWDTH16,
    WRDWDTH32
} wrd_wdth;

/* Memory section
 * A node in the memory sections list
 **/
struct mem_sect {
        SLIST_ENTRY(mem_sect) sect_lst;
        addr_t  start_addr;
        addr_t  end_addr;
        void    *mem;
};

/* Memory structure
 * Represents a physical memory componenet
 **/
struct mem {
    SLIST_HEAD(sect_list, mem_sect) sects;
    addr_t      SOM;
    addr_t      EOM;
    wrd_wdth    wrdw;
};

/* Memory word
 * A memory unit used in the memory device
 **/
union mem_word {
    uint8_t     WRD8;
    uint16_t    WRD16;
    uint32_t    WRD32;
};

//#define MEM_WRD8(VAR)   ((union mem_word)(VAR)).WRD8
//#define MEM_WRD16(VAR)  ((union mem_word)(VAR)).WRD16
//#define MEM_WRD32(VAR)  ((union mem_word)(VAR)).WRD32

/*** Generic API ***/
struct mem*         mem_init(wrd_wdth);
void                mem_free(struct mem*);

int                 mem_add_sect(struct mem*, addr_t, addr_t, void*);
struct mem_sect*    mem_get_sect(struct mem*, addr_t);

int                 mem_read_word(struct mem*, addr_t, union mem_word*);
int                 mem_write_word(struct mem*, addr_t, union mem_word*);

void                mem_info(struct mem*);

#endif
