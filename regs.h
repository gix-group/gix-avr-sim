/*****************************************************************************
*
* Project:	gix-avr-sim - AVR simulator
*
* File:     regs.h - AVR registers configuration and API header file
*
* Copyright (c) 2016, Gil Treibush <tkks085@gmail.com>
* ----------------------------------------------------
* This program is licensed under the MIT license.
*   - A copy of the license is included in a "LICENSE" file in the
*     root directory.
*
*	notes:
*   ------
*
*****************************************************************************/

#ifndef __REGS__
#define __REGS__

#include <stdint.h>
#include "mem.h"
#include "sram.h"

typedef uint8_t     REG8;
typedef uint16_t    REG16;

typedef struct {
        REG8 L;
        REG8 H;
} REG16PARTS;

typedef union {
    REG16PARTS PARTS;
    REG16 REG;
} DREG;

/*** Global registers ***/
extern REG8 IO_REGS[64];

extern REG8 *ADCSRB;

union REG_ADC {
    struct {
        REG8 ADCL;
        REG8 ADCH;
    } parts;
    REG16   all;
} *ADC;

#define ADCL ADC->parts.ADCL
#define ADCH ADC->parts.ADCH

extern REG8 *ADCSRA;
extern REG8 *ADMUX;
extern REG8 *ACSR;
extern REG8 *USICR;
extern REG8 *USISR;
extern REG8 *USIDR;
extern REG8 *USIBR;
extern REG8 *GPIOR0;
extern REG8 *GPIOR1;
extern REG8 *GPIOR2;
extern REG8 *DIDR0;
extern REG8 *PCMSK;
extern REG8 *PINB;
extern REG8 *DDRB;
extern REG8 *PORTB;
extern REG8 *EECR;
extern REG8 *EEDR;
extern REG8 *EEARL;
extern REG8 *EEARH;
extern REG8 *PRR;
extern REG8 *WDTCR;
extern REG8 *DWDR;
extern REG8 *DTPS1;
extern REG8 *DT1B;
extern REG8 *DT1A;
extern REG8 *CLKPR;
extern REG8 *PLLCSR;
extern REG8 *OCR0B;
extern REG8 *OCR0A;
extern REG8 *TCCR0A;
extern REG8 *OCR1B;
extern REG8 *GTCCR;
extern REG8 *OCR1C;
extern REG8 *OCR1A;
extern REG8 *TCNT1;
extern REG8 *TCCR1;
extern REG8 *OSCCAL;
extern REG8 *TCNT0;
extern REG8 *TCCR0B;
extern REG8 *MCUSR;
extern REG8 *MCUCR;
extern REG8 *SPMCSR;
extern REG8 *TIFR;
extern REG8 *TIMSK;
extern REG8 *GIFR;
extern REG8 *GIMSK;

union REG_SP{
    /* 8 bit Stack Pointer */
    struct {
        REG8    SPL;
        REG8    SPH;
    } parts;
    REG16   all;
} *M_SP;

/* Stack Actions */
#define SP M_SP->all
#define SPH M_SP->parts.SPH
#define SPL M_SP->parts.SPL

#define SP_PUSH_DWORD(VAL) do {         \
    sram_write_dword(SP, (uint16_t)VAL);\
    SP++;                               \
} while (0);

#define SP_PUSH_WORD(VAL) do {          \
    sram_write_word(SP, (uint8_t)VAL);  \
    SP++;                               \
} while (0);

#define SP_POP_DWORD(VAR, STATUS) do {              \
    VAR = (uint16_t)sram_read_dword(SP, &STATUS);   \
    SP--;                                           \
} while (0);

#define SP_POP_WORD(VAR, STATUS) do {               \
    VAR = (uint8_t)sram_read_word(SP, &STATUS);     \
    SP--;                                           \
} while (0);

extern union REG_SREG {
    /* 8 bit Status register */
    struct {            // SREG by Flags
        uint8_t C:1;    // Carry Flag
        uint8_t Z:1;    // Zero Flag
        uint8_t N:1;    // Negative Flag
        uint8_t V:1;    // Two's complement overflow indicator
        uint8_t S:1;    // N xor V, for signed tests
        uint8_t H:1;    // Half Carry Flag
        uint8_t T:1;    // Transfer bit used by BLD and BST
                        // instructions
        uint8_t I:1;    // Global Interrupt Enable/Disable Flag
    } flags;
    REG8 REG;
} *SREG;

#define SETSREG(BIT) SREG->REG |= ((REG8)1<<(BIT));      // (0 <= BIT <= 7)
#define CLRSREG(BIT) SREG->REG &= ~((REG8)1<<(BIT));     // (0 <= BIT <= 7)
#define TESTSREG(BIT) ((SREG->REG & ((REG8)1<<(BIT)))>0) // (0 <= BIT <= 7)

/* Flags */
#define SETFLG(FNAME) SREG->flags.FNAME = 1;
#define CLRFLG(FNAME) SREG->flags.FNAME = 0;
#define FLAG(FNAME) SREG->flags.FNAME
#define TESTFLG(FNAME) (SREG->flags.FNAME == 1)

/* Instruction registers */
extern REG16 PC;
extern REG16 INSTREG;

/* Register file */
extern REG8 REG_FILE[32];

extern uint16_t *X_REG;     // X: 16-bit register
extern uint16_t *Y_REG;     // Y: 16-bit register
extern uint16_t *Z_REG;     // Z: 16-bit register

/* Registers file macros */
#define PCINC(NUM) PC += NUM;

/* API Function */
void init_sp();

void regs_tick();

void print_flags();

#endif
